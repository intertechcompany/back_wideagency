<?php namespace Intertech\Wideagensy\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Database\Traits\Validation;
use Intertech\Wideagensy\Services\Traits\SitemapTrait;

/**
 * Model
 */
class Tour extends Model
{
    use Validation, Sluggable, SitemapTrait;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string[]
     */
    protected $slugs = ['slug' => 'title'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title' => 'required',
        'slug' => 'required|alpha_dash|between:1,255|unique:intertech_wideagensy_tours',
        'main_banner' => 'required',
    ];

    /**
     * @var \string[][]
     */
    public $attachOne = [
        'main_banner' => ['System\Models\File'],
        'member_team_image' => ['System\Models\File'],
    ];

    /**
     * @var string[]
     */
    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    /**
     * @var string[]
     */
    public $jsonable = ['member_team'];

    /**
     * @var array[]
     */
    public $translatable = [
        ['title', 'index' => true],
        ['description', 'index' => true],
        ['tour_date', 'index' => true],
        ['tour_description', 'index' => true],
        ['tour_remark', 'index' => true],
        ['main_banner_description', 'index' => true],
        ['register_banner', 'index' => true],
        ['content', 'index' => true],
        ['content_second', 'index' => true],
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_wideagensy_tours';

    protected function afterDelete()
    {
        parent::afterDelete();
        $this->generateSitemap();
    }
}
