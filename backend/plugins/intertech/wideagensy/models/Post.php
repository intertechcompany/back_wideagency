<?php namespace Intertech\Wideagensy\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Database\Traits\Validation;
use Intertech\Wideagensy\Services\Traits\SitemapTrait;

/**
 * Model
 */
class Post extends Model
{
    use Validation, Sluggable, SitemapTrait;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    protected $slugs = ['slug' => 'title'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title' => 'required',
        'slug' => 'required|alpha_dash|between:1,255|unique:intertech_wideagensy_posts',
        'poster' => 'required',
        'short_description' => 'required',
    ];

    public $attachOne = [
        'poster' => ['System\Models\File'],
        'image_1' => ['System\Models\File'],
        'image_2' => ['System\Models\File'],
        'image_3' => ['System\Models\File'],
        'image_4' => ['System\Models\File'],
        'image_5' => ['System\Models\File'],
    ];

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $jsonable = ['slider'];

    public $translatable = [
        ['title', 'index' => true],
        ['short_description', 'index' => true],
        ['label_description_1', 'index' => true],
        ['description_1', 'index' => true],
        ['description_1', 'index' => true],
        ['label_description_2', 'index' => true],
        ['description_2', 'index' => true],
        ['quote', 'index' => true],
        ['author', 'index' => true],
        ['description_3', 'index' => true],
        ['label_description_4', 'index' => true],
        ['description_4', 'index' => true],
        ['description_5', 'index' => true],
        'slider',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_wideagensy_posts';

    protected function afterDelete()
    {
        parent::afterDelete();
        $this->generateSitemap();
    }
}
