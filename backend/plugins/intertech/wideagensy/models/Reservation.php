<?php namespace Intertech\Wideagensy\Models;

use Illuminate\Support\Facades\Lang;
use Model;

/**
 * Model Reservation
 */
class Reservation extends Model
{
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [

    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'intertech_wideagensy_reservations';

    public const QUANTITY_ONE = 'one';
    public const QUANTITY_COUPLE = 'couple';
    public const QUANTITY_UNKNOWN = 'unknown';


    private function getQuantity(string $quantity): string
    {
        switch($quantity) {
            case self::QUANTITY_ONE:
                return Lang::get('intertech.wideagensy::lang.reservations.quantity_type.one');
            case self::QUANTITY_COUPLE:
                return Lang::get('intertech.wideagensy::lang.reservations.quantity_type.couple');
            default:
            case self::QUANTITY_UNKNOWN:
                return Lang::get('intertech.wideagensy::lang.reservations.quantity_type.unknown');
        }
    }

    public function getQuantityAttribute($quantity)
    {
        return $this->getQuantity($quantity);
    }

    public function getQuantityOptions()
    {
        return [
            self::QUANTITY_ONE => Lang::get('intertech.wideagensy::lang.reservations.quantity_type.one'),
            self::QUANTITY_COUPLE => Lang::get('intertech.wideagensy::lang.reservations.quantity_type.couple'),
            self::QUANTITY_UNKNOWN => Lang::get('intertech.wideagensy::lang.reservations.quantity_type.unknown'),
        ];
    }
}
