<?php

namespace Intertech\Wideagensy\Models;

use Model;

/**
 * Model
 */
class Page extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
        'System.Behaviors.SettingsModel',
    ];

    /**
     * @var string
     */
    public $settingsCode = 'intertech.wideagensy.pages';

    /**
     * @var string
     */
    public $settingsFields = 'fields.yaml';

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $jsonable = ['home_services', 'home_about', 'home_footer', 'footer_social'];

    public $translatable = [
        ['home_banner_date', 'index' => true],
        ['home_banner_tour_name', 'index' => true],
        ['home_banner_tour_description', 'index' => true],
        ['home_banner_btn_label', 'index' => true],

        ['home_description_block_name', 'index' => true],
        ['home_description_block_description', 'index' => true],
        ['home_description_block_description_1', 'index' => true],
        ['home_description_hashtag_label', 'index' => true],
        ['home_description_hashtag_link', 'index' => true],

        'home_services',
        'home_about',
        'home_footer',

        ['tour_banner_description', 'index' => true],
        ['tour_banner_btn_label', 'index' => true],

        ['footer_address', 'index' => true],

        ['footer_address', 'index' => true],
        'footer_social',

        ['home_footer_insta_label', 'index' => true],
        ['home_footer_insta_name', 'index' => true],

        'home_banner_btn_link',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'home_banner' => 'required',
        'home_banner_date' => 'required',
        'home_banner_tour_name' => 'required',
        'home_banner_tour_description' => 'required',
        'home_banner_btn_label' => 'required',
        'home_banner_btn_link' => 'required',
        'home_description_block_name' => 'required',
        'home_description_block_description' => 'required',
//        'home_description_image' => 'required',
//        'home_description_hashtag_label' => 'required',
//        'home_description_hashtag_link' => 'required',
//        'home_description_full_image' => 'required',
        'home_services' => 'required',
        'home_about' => 'required',
        'home_footer' => 'required',
//        'tour_banner' => 'required',
        'tour_banner_description' => 'required',
        'tour_banner_btn_label' => 'required',
        'tour_banner_btn_link' => 'required',
        'footer_phone' => 'required',
        'footer_email' => 'required',
        'footer_address' => 'required',
        'footer_address_link' => 'required',
    ];

    public $attachOne = [
        'home_banner' => ['System\Models\File'],
        'home_description_image' => ['System\Models\File'],
        'home_description_full_image' => ['System\Models\File'],
        'home_about_image' => ['System\Models\File'],

//        'tour_banner' => ['System\Models\File'],
    ];
}
