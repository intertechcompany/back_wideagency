<?php namespace Intertech\Wideagensy\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class TableUpdateIntertechWideagensyTours extends Migration
{
    public function up()
    {
        Schema::table('intertech_wideagensy_tours', function($table)
        {
            $table->boolean('in_home_page')->default(false);
            $table->boolean('visible')->default(true);
            $table->boolean('is_past')->default(false);
            $table->boolean('is_partner')->default(false);
            $table->boolean('is_access_on_url')->default(true);
            $table->text('register_banner')->nullable();
            $table->string('tour_price_for_one')->nullable();
            $table->string('tour_price_for_two')->nullable();
            $table->text('main_banner_description')->nullable();
            $table->text('content')->nullable();
            $table->text('content_second')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('intertech_wideagensy_tours', function($table)
        {
            $table->dropColumn('in_home_page');
            $table->dropColumn('visible');
            $table->dropColumn('is_past');
            $table->dropColumn('is_partner');
            $table->dropColumn('is_access_on_url');
            $table->dropColumn('register_banner');
            $table->dropColumn('tour_price_for_one');
            $table->dropColumn('tour_price_for_two');
            $table->dropColumn('main_banner_description');
            $table->dropColumn('content');
            $table->dropColumn('content_second');
            $table->dropTimestamps();
        });
    }
}
