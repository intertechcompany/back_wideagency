<?php namespace Intertech\Wideagensy\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class TableCreateIntertechWideagensyPosts extends Migration
{
    public function up()
    {
        if (Schema::hasTable('intertech_wideagensy_posts')) {
            return;
        }
        Schema::create('intertech_wideagensy_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug');

            $table->string('title');
            $table->text('short_description')->nullable();

            $table->text('label_description_1')->nullable();
            $table->text('description_1')->nullable();

            $table->text('label_description_2')->nullable();
            $table->text('description_2')->nullable();

            $table->text('quote')->nullable();
            $table->text('author')->nullable();

            $table->text('description_3')->nullable();

            $table->text('slider')->nullable();

            $table->text('label_description_4')->nullable();
            $table->text('description_4')->nullable();
            $table->text('description_5')->nullable();

            $table->boolean('in_home_page')->default(false);
            $table->boolean('visible')->default(true);
            $table->boolean('is_access_on_url')->default(true);


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('intertech_wideagensy_posts');
    }
}
