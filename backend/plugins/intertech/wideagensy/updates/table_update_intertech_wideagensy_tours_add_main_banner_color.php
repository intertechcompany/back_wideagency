<?php namespace Intertech\Wideagensy\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class TableUpdateIntertechWideagensyToursAddMainBannerColor extends Migration
{
    public function up()
    {
        Schema::table('intertech_wideagensy_tours', function($table)
        {
            $table->string('main_banner_color')->nullable();
            $table->boolean('is_default_main_banner_color')->default(true);
        });
    }

    public function down()
    {
        Schema::table('intertech_wideagensy_tours', function($table)
        {
            $table->dropColumn('main_banner_color');
            $table->dropColumn('is_default_main_banner_color');
        });
    }
}
