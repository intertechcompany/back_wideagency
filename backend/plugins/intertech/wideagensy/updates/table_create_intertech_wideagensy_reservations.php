<?php namespace Intertech\Wideagensy\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class TableCreateIntertechWideagensyReservations extends Migration
{
    public function up()
    {
        if (Schema::hasTable('intertech_wideagensy_reservations')) {
            return;
        }
        Schema::create('intertech_wideagensy_reservations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('comment')->nullable();
            $table->string('quantity');


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('intertech_wideagensy_reservations');
    }
}
