<?php namespace Intertech\Wideagensy\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class TableUpdateIntertechWideagensyToursAddIsNoPlaces extends Migration
{
    public function up()
    {
        Schema::table('intertech_wideagensy_tours', function($table)
        {
            $table->boolean('is_no_places')->default(false);
        });
    }

    public function down()
    {
        Schema::table('intertech_wideagensy_tours', function($table)
        {
            $table->dropColumn('is_no_places');
        });
    }
}
