<?php namespace Intertech\Wideagensy\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class TableCreateIntertechWideagensyTours extends Migration
{
    public function up()
    {
        if (Schema::hasTable('intertech_wideagensy_tours')) {
            return;
        }
        Schema::create('intertech_wideagensy_tours', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug');

            $table->string('title')->nullable();
            $table->text('member_team')->nullable();
            $table->string('tour_remark')->nullable();
            $table->string('tour_date')->nullable();
            $table->text('tour_description')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('intertech_wideagensy_tours');
    }
}
