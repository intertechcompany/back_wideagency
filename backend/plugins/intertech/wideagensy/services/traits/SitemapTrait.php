<?php

namespace Intertech\Wideagensy\Services\Traits;

use October\Rain\Support\Facades\Flash;

/**
 * Trait SitemapTrait
 *
 * @package Intertech\Wideagensy\Services\Traits
 */
trait SitemapTrait
{
    public function generateSitemap()
    {
        $url = config('services.app.front.url') . config('services.app.front.sitemap');

        $options = [
            'http' => [
                'method' => 'POST',
            ],
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        if(false === $result) {
            Flash::error('Sitemap generation error');
        }
    }
}
