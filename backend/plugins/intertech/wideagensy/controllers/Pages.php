<?php

namespace Intertech\Wideagensy\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Intertech\Wideagensy\Models\Page;

/**
 * Description Settings Back-end Controller
 */
class Pages extends Controller
{
    /**
     * @var string[]
     */
    public $implement = [
        'Backend.Behaviors.FormController'
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';


    /**
     * Pages constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Intertech.Wideagensy', 'wideagensy', 'wideagensy-pages');
    }

    /**
     *
     */
    public function index()
    {
        $this->asExtension('FormController')->update();
    }

    /**
     * @return mixed
     */
    public function formFindModelObject()
    {
        return Page::instance();
    }

    /**
     * @return mixed
     */
    public function index_onSave()
    {
        return $this->asExtension('FormController')->update_onSave();
    }
}
