<?php namespace Intertech\Wideagensy\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Class Reservations
 *
 * @package Intertech\Wideagensy\Controllers
 */
class Reservations extends Controller
{
    /**
     * @var string[]
     */
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';
    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';

    /**
     * Reservations constructor.
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Intertech.Wideagensy', 'wideagensy', 'wideagensy-reservations');
    }
}
