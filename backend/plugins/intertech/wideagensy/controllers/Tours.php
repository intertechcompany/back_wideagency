<?php namespace Intertech\Wideagensy\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Intertech\Wideagensy\Services\Traits\SitemapTrait;

/**
 * Class Tours
 *
 * @package Intertech\Wideagensy\Controllers
 */
class Tours extends Controller
{
    use SitemapTrait;

    /**
     * @var string[]
     */
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';
    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';

    /**
     * Tours constructor.
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Intertech.Wideagensy', 'wideagensy', 'wideagensy-tours');
    }

    /**
     * @param $model
     */
    public function formAfterCreate($model)
    {
        $this->generateSitemap();
    }

    /**
     * @param $model
     */
    public function formAfterSave($model)
    {
        $changes = $model->getChanges();

        if(array_key_exists('is_access_on_url', $changes)) {
            $this->generateSitemap();
        }
    }

    /**
     * @param $model
     */
    public function formAfterDelete($model)
    {
        $this->generateSitemap();
    }
}
