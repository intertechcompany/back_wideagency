<?php return [
    'plugin' => [
        'name' => 'SEO',
        'description' => '',
    ],
    'robots' => [
        'label' => 'Robots.txt',
        'content' => 'Content',
    ],
    'seo' => [
        'external' => [
            'url' => 'Hyperlink',
            'url_comment' => 'Template with the ability to use a mask without specifying the language For example: /, / news, / news-some-news-href',
            'head' => 'In &lt;head&gt;',
            'body_top' => 'At the beginning &lt;body&gt;',
            'body_bottom' => 'At the end &lt;body&gt;',
            'is_active' => 'On site',
        ],
        'redirect' => [
            'old_url' => 'Old link',
            'new_url' => 'New link',
            'url_comment' => 'For example  - /url',
            'is_enabled' => 'Visibility',
            'counter' => [
                'label' => 'Counter',
                'comment' => 'Number of transitions',
            ],
            'created_at' => 'Creation date',
            'updated_at' => 'Updated date',
        ],
        'meta' => [
            'seo_url_type' => [
                'label' => 'Link Type',
                'options' => [
                    'route' => 'Route',
                    'link' => 'Arbitrary link',
                ],
            ],
            'route' => 'Route',
            'url_mask' => 'Hyperlink',
            'url_comment' => 'Template with the ability to use a mask without specifying the language For example: /, /news, /news-some-news-href',
            'title' => [
                'label' => 'Title',
                'comment' => 'Page title template. For example: "Welcome: {{$ item->title}}‚ {{$ item->description}} "Simple Blade template syntax is available: https://laravel.com/docs/6.0/blade ',
            ],
            'meta_tags' => 'Additional meta tags',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'is_indexing' => 'Indexed',
            'is_active' => 'Enabled on the site',
            'default_tab' => 'Meta',
            'canonical' => 'Canonical',
            'og_tab' => 'Open Graph',
            'og_title' => 'Title',
            'og_description' => 'Description',
            'og_image' => 'Image',
        ],
        'section' => [
            'seo_desc' => 'List of values for dynamic seo data generation: {{ $item->title }} {{ $item->description }} {{ $item->keywords }} {{ $item->canonical }} {{ $item->image }} {{ $item->meta_tags }}',
        ],
    ],
    'navigation' => [
        'tags' => 'Meta tags',
        'external' => 'External scripts',
        'redirect' => 'Redirect',
        'robots' => 'Robots.txt',
    ],
    'permissions' => [
        'list' => 'List of SEO parameters',
        'create' => 'Add SEO parameters',
        'edit' => 'Editing SEO parameters',
        'delete' => 'Deleting SEO parameters',
        'settings' => 'Edit default SEO settings',
        'redirect' => 'Editing redirects',
    ],
    'tabs' => [
        'seo' => 'Seo',
    ],
];
