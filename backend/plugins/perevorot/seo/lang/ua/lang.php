<?php return [
    'plugin' => [
        'name' => 'SEO',
        'description' => ''
    ],
    'robots' => [
        'label' => 'Robots.txt',
        'content' => 'Контент',
    ],
    'seo' => [
        'external' => [
            'url' => 'Гіперпосилання',
            'url_comment' => 'Шаблон з можливістю використання маски без вказівки мови. Наприклад: /, /news, /news-some-news-href',
            'head' => 'В &lt;head&gt;',
            'body_top' => 'На початку &lt;body&gt;',
            'body_bottom' =>  'В кінці &lt;body&gt;',
            'is_active' =>  'На сайті',
        ],
        'redirect' => [
            'old_url' => 'Старе посилання',
            'new_url' => 'Нове посилання',
            'url_comment' => 'Наприклад - /url',
            'is_enabled' => 'Видимість',
            'counter' => [
                'label' => 'Лічильник',
                'comment' => 'Кількість переходів',
            ],
            'created_at' => 'Дата створення',
            'updated_at' => 'Дата поновлення',
        ],
        'meta' => [
            'seo_url_type' => [
                'label' => 'Тип гіперпосилання',
                'options' => [
                    'route' => 'Роут',
                    'link' => 'Довільне гіперпосилання',
                ],
            ],
            'route' => 'Роут',
            'url_mask' => 'Гіперпосилання',
            'url_comment' => 'Шаблон з можливістю використання маски без вказівки мови. Наприклад: /, /news, /news-some-news-href',
            'title' => [
                'label' => 'Назва',
                'comment' => 'Шаблон заголовка сторінки. Наприклад: «Ласкаво просимо: {{$ item->title}}, {{$ item->description}}» Доступно використання простого синтаксису шаблонів Blade: https://laravel.com/docs/6.0/blade ',
            ],
            'description' => 'Опис',
            'meta_tags' => 'Додаткові meta теги',
            'keywords' => 'Ключові слова',
            'is_indexing' => 'Індексується',
            'is_active' => 'Включено на сайті',
            'default_tab' => 'Meta',
            'canonical' => 'Canonical',
            'og_tab' => 'Open Graph',
            'og_title' => 'Заголовок',
            'og_description' => 'Опис',
            'og_image' => 'Зображення',
        ],
        'section' => [
            'seo_desc' => 'Перелік значень для динамічної генерації seo даних: {{ $item->title }} {{ $item->description }} {{ $item->keywords }} {{ $item->canonical }} {{ $item->image }} {{ $item->meta_tags }}',
        ],
    ],
    'navigation' => [
        'tags' => 'Meta-теги',
        'external' => 'Зовнішні скрипти',
        'redirect' => 'Переадресація',
        'robots' => 'Robots.txt',
    ],
    'permissions' => [
        'list' => 'Список SEO параметрів',
        'create' => 'Додавання SEO параметрів',
        'edit' => 'Редагування SEO параметрів',
        'delete' => 'Вилучення SEO параметрів',
        'settings' => 'Редагування SEO налаштувань за замовчуванням',
        'redirect' => 'Редагування переадресаций',
    ],
    'tabs' => [
        'seo' => 'Seo',
    ],
];
