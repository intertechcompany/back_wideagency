<?php return [
    'plugin' => [
        'name' => 'SEO',
        'description' => ''
    ],
    'robots' => [
        'label' => 'Robots.txt',
        'content' => 'Контент',
    ],
    'seo' => [
        'external' => [
            'url' => 'Гиперссылка',
            'url_comment' => 'Шаблон с возможностью использования маски без указания языка. Например: /, /news, /news-some-news-href',
            'head' => 'В &lt;head&gt;',
            'body_top' => 'В начале &lt;body&gt;',
            'body_bottom' =>  'В конце &lt;body&gt;',
            'is_active' =>  'На сайте',
        ],
        'redirect' => [
            'old_url' => 'Старая ссылка',
            'new_url' => 'Новая ссылка',
            'url_comment' => 'Например - /url',
            'is_enabled' => 'Видимость',
            'counter' => [
                'label' => 'Счетчик',
                'comment' => 'Количество переходов',
            ],
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ],
        'meta' => [
            'seo_url_type' => [
                'label' => 'Тип гиперссылки',
                'options' => [
                    'route' => 'Роут',
                    'link' => 'Произвольная ссылка',
                ],
            ],
            'route' => 'Роут',
            'url_mask' => 'Гиперссылка',
            'url_comment' => 'Шаблон с возможностью использования маски без указания языка. Например: /, /news, /news-some-news-href',
            'title' => [
                'label' => 'Название',
                'comment' => 'Шаблон заголовка страницы. Например: «Добро пожаловать: {{ $item->title }}‚ {{ $item->description }}» Доступно использование простого синтаксиса шаблонов Blade: https://laravel.com/docs/6.0/blade',
            ],
            'description' => 'Описание',
            'meta_tags' => 'Дополнительные meta теги',
            'keywords' => 'Ключевые слова',
            'is_indexing' => 'Индексируется',
            'is_active' => 'Включено на сайте',
            'default_tab' => 'Meta',
            'canonical' => 'Canonical',
            'og_tab' => 'Open Graph',
            'og_title' => 'Заголовок',
            'og_description' => 'Описание',
            'og_image' => 'Изображение',
        ],
        'section' => [
            'seo_desc' => 'Перечень значений для динамической генерации seo данных: {{ $item->title }} {{ $item->description }} {{ $item->keywords }} {{ $item->canonical }} {{ $item->image }} {{ $item->meta_tags }}',
        ],
    ],
    'navigation' => [
        'tags' => 'Meta-теги',
        'external' => 'Внешние скрипты',
        'redirect' => 'Переадресация',
        'robots' => 'Robots.txt',
    ],
    'permissions' => [
        'list' => 'Список SEO параметров',
        'create' => 'Добавление SEO параметров',
        'edit' => 'Редактирование SEO параметров',
        'delete' => 'Удаление SEO параметров',
        'settings' => 'Редактирование SEO настроек по умолчанию',
        'redirect' => 'Редактирование переадресаций',
    ],
    'tabs' => [
        'seo' => 'Seo',
    ],
];
