<?php

namespace Perevorot\Seo\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Perevorot\Seo\Models\Robot;

/**
 * Description Settings Back-end Controller
 */
class Robots extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController'
    ];

    public $formConfig = 'config_form.yaml';


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Perevorot.Seo', 'seo', 'robots');
    }

    public function index()
    {
        $this->asExtension('FormController')->update();
    }

    public function formFindModelObject()
    {
        return Robot::instance();
    }

    public function index_onSave()
    {
        return $this->asExtension('FormController')->update_onSave();
    }

}
