<?php namespace Perevorot\Seo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotSeoSeo7 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_seo_seo', function($table)
        {
            $table->dropColumn('image');
            $table->string('url_mask')->nullable()->change();
            $table->string('title')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->text('keywords')->nullable()->change();
            $table->string('canonical')->nullable()->change();
            $table->text('meta_tags')->nullable()->change();
            $table->string('og_title')->nullable()->change();
            $table->text('og_description')->nullable()->change();
            $table->text('route')->nullable()->change();
            $table->boolean('seo_url_type')->default(0)->change();

        });
    }

    public function down()
    {
        Schema::table('perevorot_seo_seo', function($table)
        {
            $table->string('image');
            $table->string('url_mask')->change();
            $table->string('title')->change();
            $table->text('description')->change();
            $table->text('keywords')->change();
            $table->string('keywords')->change();
            $table->string('string')->change();
            $table->text('meta_tags')->change();
            $table->string('og_title')->change();
            $table->text('og_description')->change();
            $table->text('route')->change();
            $table->boolean('seo_url_type')->change();
        });
    }
}
