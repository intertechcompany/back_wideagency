<?php

namespace Perevorot\Seo\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Model
 */
class Robot extends Model
{
    use Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
        'System.Behaviors.SettingsModel',
    ];

    /**
     * @var string
     */
    public $settingsCode = 'intertech.wideagensy.robot';

    /**
     * @var string
     */
    public $settingsFields = 'fields.yaml';

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;


    /**
     * @var array Validation rules
     */
    public $rules = [
        'content' => 'required',
    ];
}
