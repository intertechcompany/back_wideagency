<?php

namespace Perevorot\Seo\Models;

use Model;
use Perevorot\Page\Traits\CacheClear;
use Perevorot\Seo\Traits\SeoEventsTrait;
use Perevorot\Seo\Traits\SeoPublicRoutesTrait;

/**
 * Model
 */
class External extends Model
{
    use SeoEventsTrait;
    use SeoPublicRoutesTrait;
    use CacheClear;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_seo_external';
}
