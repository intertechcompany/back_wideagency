<?php

use Illuminate\Support\Facades\DB;

$routeGroup=[
    'prefix' => Localization::setLocale(),
    'middleware' => [
        //'localeSessionRedirect',
        'localizationRedirect',
        'localeViewPath',
        'redirectTrailingSlash',
        'caching'
    ]
];

Route::get('/cache/clear', function() {
    \Artisan::call('cache:clear');
})->name('cache.clear');


Route::group($routeGroup, function () {
    Route::get('robots.txt', function() {
        $robots = DB::table('system_settings')->where('item', '=', 'intertech.wideagensy.robot')->first();
        if(null !== $robots && !empty($robots)) {
            $content = json_decode($robots->value);
            $headers = ['Content-type'=>'text/plain','Content-Length'=>strlen($content->content)];
            return response()->make($content->content, 200, $headers);

        }
    });

	Route::get('/', 'PageController@homepage')->name('home.page');

	Route::get('/posts', 'PageController@posts')->name('posts');
	Route::get('/posts/{slug}', 'PageController@post')->name('posts.page');

	Route::get('/tours', 'PageController@tours')->name('tours');
	Route::get('/tours/{slug}', 'PageController@tour')->name('tours.page');

	Route::post('/tours/reservation', 'ReservationController@save')->name('tours.reservation');

    Route::get('{slug}', 'PageController@page')->name('page');
});
