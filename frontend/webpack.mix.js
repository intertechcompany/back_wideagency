const mix = require('laravel-mix');

mix.js([
    'resources/assets/js/app.js',
], 'public/js/app.js').autoload({
    jquery: ['$', 'window.jQuery', 'jQuery', 'jquery'],
})
    // Custom
    .js([
        'resources/assets/js/scripts/libs/slick.min.js',
        'resources/assets/js/scripts/custom.js',
    ], 'public/js/custom.js')

    // Tours
    .js([
        'resources/assets/js/scripts/libs/slick.min.js',
        'resources/assets/js/scripts/tours.js',
    ], 'public/js/tours.js')

    // Tour
    .js([
        'resources/assets/js/scripts/tour.js',
    ], 'public/js/tour.js')

    // Post
    .js([
        'resources/assets/js/scripts/libs/slick.min.js',
        'resources/assets/js/scripts/article.js',
    ], 'public/js/article.js')


    //Main
    .sass('resources/assets/sass/app.scss', 'public/css').options({
        processCssUrls: false
    })
    // Homepage
    .sass('resources/assets/sass/main-page.scss', 'public/css').options({
        processCssUrls: false
    })
    // Tours page
    .sass('resources/assets/sass/tours.scss', 'public/css').options({
        processCssUrls: false
    })
    // Tour page
    .sass('resources/assets/sass/tour.scss', 'public/css').options({
        processCssUrls: false
    })
    // Posts page
    .sass('resources/assets/sass/blog.scss', 'public/css').options({
        processCssUrls: false
    })
    // Post page
    .sass('resources/assets/sass/article.scss', 'public/css').options({
        processCssUrls: false
    })
    // 404 page
    .sass('resources/assets/sass/404.scss', 'public/css').options({
        processCssUrls: false
    })
;

mix.copyDirectory('resources/assets/img', 'public/img');
mix.copyDirectory('resources/assets/fonts', 'public/fonts');

if (mix.inProduction()) {
    mix.version();
}

if (!mix.inProduction()) {
    mix.sourceMaps();
}