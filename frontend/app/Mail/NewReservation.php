<?php

namespace App\Mail;

use App\Models\Reservation;
use App\Models\Settings;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NewReservation
 *
 * @package App\Mail
 */
class NewReservation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Reservation
     */
    public $reservation;

    /**
     * Create a new message instance.
     *
     * @param Reservation $reservation
     */
    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): ?self
    {
        $settings = Settings::instance();

        if(null !== $settings) {
            $value = json_decode($settings->value, true);

            if(!empty($value) && isset($value['email_from'])) {
                return $this->from($value['email_from'])->view('emails.reservation.new');
            }
        }
    }
}
