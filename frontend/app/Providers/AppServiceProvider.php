<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'Perevorot\Maksymkotsiuba\Models\Project' => 'App\Models\Project',
            'Perevorot\Maksymkotsiuba\Models\Text' => 'App\Models\Text',
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $allLocales = config('laravellocalization.supportedLocales');
        $activeLocales = DB::table('rainlab_translate_locales')
            ->where('is_enabled', 1)
            ->orderBy('sort_order', 'asc')
            ->get();

        if(!$activeLocales->isEmpty()) {
            $setLocales = [];

            foreach ($activeLocales as $locale) {
                if(isset($allLocales[$locale->code])) {
                    $setLocales[$locale->code] = $allLocales[$locale->code];
                }
            }

            $defLocale = $activeLocales->where('is_default', 1)->first();

            if($defLocale) {
                config([
                    'app.locale' => $defLocale->code,
                    'app.fallback_locale' => $defLocale->code,
                ]);
            }

            if(!empty($setLocales)) {
                config([
                    'laravellocalization.supportedLocales' => $setLocales,
                    'laravellocalization.useAcceptLanguageHeader' => true,
                    'laravellocalization.hideDefaultLocaleInURL' => true,
                    'laravellocalization.localesOrder' => array_keys($setLocales)
                ]);
            }
        }
    }
}
