<?php

namespace App\Http\Middleware;

use App\Services\Helpers\SeoHelper;
use Closure;
use Illuminate\Http\Request;

/**
 * Class Seo
 *
 * @package App\Http\Middleware
 */
class Seo
{
    /**
     * @var SeoHelper
     */
    private $seoHelper;

    /**
     * Seo constructor.
     *
     * @param SeoHelper $seoHelper
     */
    public function __construct(SeoHelper $seoHelper)
    {
        $this->seoHelper = $seoHelper;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $seo = $this->seoHelper->getSeo($request->route()->getName(), $request->path());

        if(null !== $seo) {
            $request->request->add([
                'seo' => $seo,
            ]);
        }

        return $next($request);
    }
}
