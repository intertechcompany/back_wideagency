<?php

namespace App\Http\Middleware;

use Closure;
use Stevebauman\Location\Facades\Location;
use Request;

class LocaleRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if('/cache/clear' !== $request->getPathInfo()) {
            $position = Location::get(Request::ip());


            if(!session()->has('localeByIp')) {
                if(null !== $position && false !== $position && 'UA' !== $position->countryCode) {
                    \Session::put('localeByIp', 'en');
                    app()->setLocale('en');
                } else {
                    \Session::put('localeByIp', 'ua');
                    app()->setLocale('ua');
                }

                return redirect(app('laravellocalization')->getLocalizedURL(app()->getLocale(), $request->fullUrl()));
            }
        }

        return $next($request);
    }

}