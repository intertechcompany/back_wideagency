<?php

namespace App\Http\Middleware;

use Closure;

class Redirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $redirect = \App\Models\Redirect::where([['old_url', '=', $request->getPathInfo()], ['is_enabled', '=', true]])->first();

        if(null !== $redirect) {
            $redirect->counter ++;
            $redirect->save();

            return redirect($redirect->new_url);
        }

        return $next($request);
    }
}
