<?php

namespace App\Http\Controllers;


use App\Services\Handlers\ReservationHandler;
use Illuminate\Http\Request;

/**
 * Class ReservationController
 *
 * @package App\Http\Controllers
 */
class ReservationController extends BaseController
{
    /**
     * @var ReservationHandler
     */
    private $reservationHandler;

    /**
     * ReservationController constructor.
     *
     * @param ReservationHandler $reservationHandler
     */
    public function __construct(ReservationHandler $reservationHandler)
    {
        parent::__construct();

        $this->reservationHandler = $reservationHandler;
    }

    public function save(Request $request)
    {
        $response = $this->reservationHandler->createReservation($request);

        return response()->json($response);
    }

}
