<?php

namespace App\Http\Controllers;

use App\Services\Handlers\Page\HomePageHandler;
use App\Services\Handlers\Page\PostsPageHandler;
use App\Services\Handlers\Page\ToursPageHandler;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PageController
 *
 * @package App\Http\Controllers
 */
class PageController extends BaseController
{
    /**
     * @var HomePageHandler
     */
    private $homePageHandler;

    /**
     * @var ToursPageHandler
     */
    private $toursPageHandler;

    /**
     * @var PostsPageHandler
     */
    private $postsPageHandler;

    /**
     * PageController constructor.
     *
     * @param HomePageHandler $homePageHandler
     * @param ToursPageHandler $toursPageHandler
     * @param PostsPageHandler $postsPageHandler
     */
    public function __construct(HomePageHandler $homePageHandler, ToursPageHandler $toursPageHandler, PostsPageHandler $postsPageHandler)
    {
        parent::__construct();

        $this->homePageHandler = $homePageHandler;
        $this->toursPageHandler = $toursPageHandler;
        $this->postsPageHandler = $postsPageHandler;
    }

    /**
     * @return Factory|View
     */
    public function homepage()
    {
        $response = $this->homePageHandler->getData();

        return $this->view('pages/homepage', ['data' => $response]);
    }

    /**
     * @return Factory|View
     */
    public function tours()
    {

        $response = $this->toursPageHandler->getListData();

        return $this->view('pages.tours.all', ['data' => $response]);
    }

    /**
     * @return Factory|View
     */
    public function posts()
    {
        $response = $this->postsPageHandler->getListData();

        return $this->view('pages.posts.all', ['data' => $response]);
    }

    /**
     * @param Request $request
     * @param string $slug
     *
     * @return Factory|View
     */
    public function post(Request $request, string $slug)
    {
        $response = $this->postsPageHandler->getPostData($slug);

        if(!$response['success'] && 404 === $response['status']) {
            throw new NotFoundHttpException();
        }

        return $this->view('pages.posts.one', ['data' => $response]);
    }

    /**
     * @param Request $request
     * @param string $slug
     *
     * @return Factory|View
     */
    public function tour(Request $request, string $slug)
    {

        $response = $this->toursPageHandler->getTourData($slug);

        if(!$response['success'] && 404 === $response['status']) {
            throw new NotFoundHttpException();
        }

        return $this->view('pages.tours.one', ['data' => $response]);
    }

    /**
     * @return Factory|View
     */
    public function page()
    {
        return $this->view('pages/page');
    }
}
