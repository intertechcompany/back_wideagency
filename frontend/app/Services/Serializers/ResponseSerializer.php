<?php

namespace App\Services\Serializers;

/**
 * Class ResponseSerializer
 *
 * @package App\Services\Serializers
 */
class ResponseSerializer
{
    /**
     * @param bool $success
     * @param int $status
     * @param $data
     * @param array|null $errors
     * @param array|null $message
     *
     * @return array
     */
    public function serialize(bool $success, int $status, $data, ?array $errors = [], ?array $message = []): array
    {
        return [
            'success' => $success,
            'status' => $status,
            'data' => $data,
            'errors' => $errors,
            'message' => $message,
        ];
    }
}
