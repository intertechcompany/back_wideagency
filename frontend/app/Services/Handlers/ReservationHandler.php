<?php

namespace App\Services\Handlers;

use App\Mail\NewReservation;
use App\Models\Reservation;
use App\Models\Settings;
use App\Services\Serializers\ResponseSerializer;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Mail;

/**
 * Class ReservationHandler
 *
 * @package App\Services\Handlers
 */
class ReservationHandler
{
    /**
     * @var ResponseSerializer
     */
    private $serializer;


    /**
     * OrderHandler constructor.
     *
     * @param ResponseSerializer $serializer
     */
    public function __construct(ResponseSerializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function createReservation(Request $request): ?array
    {
        try {
            $data = $this->validate($request, 'reservation');

            $reservation = new Reservation();
            $reservation->name = $data['name'];
            $reservation->email = $data['email'];
            $reservation->phone = $data['phone'];
            $reservation->comment = $data['comment'] ?? null;
            $reservation->quantity = $data['quantity'];
            $reservation->save();

            try {
                $this->sendMail($reservation);
            } catch(\Exception $exception) {
                return $this->serializer->serialize(true, 500, []);
            }


            return $this->serializer->serialize(true, 200, []);
        } catch(ValidationException $exception) {
            return $this->serializer->serialize(false, $exception->status, [], $exception->errors(), [$exception->getMessage()]);
        }
    }


    /**
     * @param Reservation $reservation
     */
    private function sendMail(Reservation $reservation): void
    {
        $settings = Settings::instance();
        if(null !== $settings) {
            $value = json_decode($settings->value, true);

            if(!empty($value) && isset($value['emails_to'])) {
                Mail::to($value['emails_to'])->send(new NewReservation($reservation));
            }
        }
    }

    /**
     * @param Request $request
     *
     * @param string $type
     *
     * @return array
     */
    private function validate(Request $request, string $type): array
    {
        if($type === 'reservation') {
            $rules = $this->getReservationRules($request);
        } else {
            return null;
        }

        return $request->validate($rules);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function getReservationRules(Request $request): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'phone' => 'required',
            'comment' => 'string|nullable',
            'quantity' => 'string',
            'agreement' => 'accepted',
        ];
    }
}