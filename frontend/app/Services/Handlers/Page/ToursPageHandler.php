<?php

namespace App\Services\Handlers\Page;

use App\Models\Page;
use App\Models\Tour;
use App\Services\Serializers\ResponseSerializer;


/**
 * Class ToursPageHandler
 *
 * @package App\Services\Handlers
 */
class ToursPageHandler
{
    /**
     * @var ResponseSerializer
     */
    private $serializer;

    /**
     * ToursPageHandler constructor.
     *
     * @param ResponseSerializer $serializer
     */
    public function __construct(ResponseSerializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return array|null
     */
    public function getListData(): ?array
    {
        try {
            $pastTours = Tour::where([['is_past', '=', true], ['is_partner', '=', false], ['visible', '=', true]])->orderBy('id', 'desc');
            $tour = Tour::where([['is_past', '=', false], ['is_partner', '=', false], ['visible', '=', true]])->orderBy('id', 'desc');

            $data = [
                'settings' => Page::instance(),
                'tours' => $tour->get(),
                'tours_count' => $tour->count() ?? 0,
                'past_tours' => $pastTours->get(),
                'past_tours_count' => $pastTours->count() ?? 0,
                'partner_tours' => Tour::where([['is_partner', '=', true], ['visible', '=', true]])->get(),
            ];

            return $this->serializer->serialize(true, 200, $data);
        } catch(\Exception $exception) {
            return $this->serializer->serialize(false, 500, [], [], [$exception->getMessage()]);
        }
    }

    /**
     * @param string $slug
     *
     * @return array|null
     */
    public function getTourData(string $slug) {
        try {

            $tour = Tour::where([['slug', '=', $slug], ['is_access_on_url', '=', true]])->first();
            if(null === $tour) {
                return $this->serializer->serialize(false, 404, []);
            }

            $data = [
                'settings' => Page::instance(),
                'tour' => $tour,
            ];

            return $this->serializer->serialize(true, 200, $data);
        } catch(\Exception $exception) {
            return $this->serializer->serialize(false, 500, [], [], [$exception->getMessage()]);
        }
    }
}
