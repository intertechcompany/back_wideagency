<?php

namespace App\Services\Handlers\Page;

use App\Models\Page;
use App\Models\Post;
use App\Models\Tour;
use App\Services\Serializers\ResponseSerializer;


/**
 * Class PostsPageHandler
 *
 * @package App\Services\Handlers
 */
class PostsPageHandler
{
    /**
     * @var ResponseSerializer
     */
    private $serializer;

    /**
     * PostsPageHandler constructor.
     *
     * @param ResponseSerializer $serializer
     */
    public function __construct(ResponseSerializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return array|null
     */
    public function getListData(): ?array
    {
        try {
            $data = [
                'settings' => Page::instance(),
                'posts' => Post::where([['is_access_on_url', '=', true], ['visible', '=', true]])->orderBy('id', 'desc')->paginate(Post::PER_PAGE),
                'tours' => Tour::where([['is_access_on_url', '=', true], ['visible', '=', true], ['is_past', '=', false], ['is_partner', '=', false]])->orderBy('id', 'desc')->limit(Tour::ON_POST_PAGE)->get(),
            ];

            return $this->serializer->serialize(true, 200, $data);
        } catch(\Exception $exception) {
            return $this->serializer->serialize(false, 500, [], [], [$exception->getMessage()]);
        }
    }

    /**
     * @param string $slug
     *
     * @return array|null
     */
    public function getPostData(string $slug) {
        try {

            $post = Post::where([['slug', '=', $slug], ['is_access_on_url', '=', true]])->first();
            if(null === $post) {
                return $this->serializer->serialize(false, 404, []);
            }

            $data = [
                'settings' => Page::instance(),
                'post' => $post,
                'next' => $post->next(),
                'previous' => $post->previous(),
            ];

            return $this->serializer->serialize(true, 200, $data);
        } catch(\Exception $exception) {
            return $this->serializer->serialize(false, 500, [], [], [$exception->getMessage()]);
        }
    }
}
