<?php

namespace App\Services\Handlers\Page;

use App\Models\Post;
use App\Models\Page;
use App\Models\Tour;
use App\Services\Serializers\ResponseSerializer;


/**
 * Class HomePageHandler
 *
 * @package App\Services\Handlers
 */
class HomePageHandler
{
    /**
     * @var ResponseSerializer
     */
    private $serializer;

    /**
     * HomePageHandler constructor.
     *
     * @param ResponseSerializer $serializer
     */
    public function __construct(ResponseSerializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return array|null
     */
    public function getData(): ?array
    {
        try {
            $data = [
                'settings' => Page::instance(),
                'tours' => Tour::where([['in_home_page', '=', true], ['is_access_on_url', '=', true], ['visible', '=', true], ['is_past', '=', false], ['is_partner', '=', false]])->limit(Tour::ON_HOME_PAGE)->get(),
                'posts' => Post::where([['in_home_page', '=', true], ['is_access_on_url', '=', true], ['visible', '=', true]])->limit(Post::ON_HOMEPAGE)->get(),
            ];

            return $this->serializer->serialize(true, 200, $data);
        } catch(\Exception $exception) {
            return $this->serializer->serialize(false, 500, [], [], [$exception->getMessage()]);
        }
    }
}
