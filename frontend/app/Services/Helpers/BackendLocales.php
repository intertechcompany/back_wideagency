<?php

namespace App\Services\Helpers;

use Exception;
use App\Models\Locale;

/**
 * Class BackendLocales
 *
 * @package App\Services\Helpers
 */
class BackendLocales
{
    /**
     * @return BackendLocales
     */
    public static function factory()
    {
        return new self;
    }

    /**
     * @param array $locales
     *
     * @return array
     */
    public function getSupportedLocales($locales = []): array
    {
        foreach ($this->getLocales() as $locale) {
            try {
                if (!array_get($locale, 'code')) {
                    continue;
                }

                $locales[array_get($locale, 'code')] = $locale;
            } catch (Exception $ex) {}
        }

        return array_filter($locales);
    }

    /**
     * @return array
     */
    public function getLocales(): array
    {
        return Locale::select('code')->enabled()->get()->toArray();
    }

    /**
     * @return string
     */
    public function getDefaultLocale(): string
    {
        $locale = Locale::select('code')
            ->where('is_default', '=', true)
            ->enabled()->first();
        if(null !== $locale) {
            return $locale->code;
        }

        return 'ua';
    }
}
