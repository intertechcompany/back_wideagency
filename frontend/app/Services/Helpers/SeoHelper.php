<?php

namespace App\Services\Helpers;

use App\Models\Seo;
use Localization;

/**
 * Class SeoHelper
 *
 * @package App\Services\Helpers
 */
class SeoHelper
{
    /**
     * @param string|null $route
     * @param string|null $path
     *
     * @return Seo|null
     */
    public function getSeo(?string $route, ?string $path): ?Seo
    {
        if(null === $route || null === $path) {
            return null;
        }

        return Seo::where(function($q) use($route, $path) {
            $q->where(function($q) use($route) {
                if($route) {
                    $q->where('seo_url_type', 0);
                    $q->where(function($q) use($route) {
                        $q->where('route', $route);
                        $q->orWhere('route', '');
                    });
                }
            })->orWhere(function($q) use ($path) {
                $q->where('seo_url_type', 1);
                $q->whereIn('url_mask', $this->getPossibleUrl($path));
            });
        })->where('is_active', '=', true)->first();
    }

    /**
     * @param string $path
     *
     * @return string[]|null
     */
    public function getPossibleUrl(string $path): ?array
    {
        if(Localization::getDefaultLocale() != Localization::getCurrentLocale()) {
            $url = ltrim($path, '/' . Localization::getCurrentLocale());
        }

        $url = '/' . trim($path, '/');

        $urls = [$url];
        $segments = preg_split("/[\/,-]+/", $url);
        $str = str_split($url);

        $dividers = [
            '/' => [],
            '-' => [],
        ];

        foreach($str as $k => $one) {
            if(in_array($one, ['/', '-'])) {
                $dividers[$one][] = $k;
            }
        }

        $i = sizeof($segments);

        array_pop($segments);

        while($i > 1) {
            $urls[] = implode('/', $segments) . '/*';
            array_pop($segments);
            $i--;
        }

        foreach($urls as $key => $url) {
            foreach(['/', '-'] as $divider) {
                foreach($dividers[$divider] as $char_position) {
                    if(strlen($url) >= $char_position) {
                        $url = substr_replace($url, $divider, $char_position, 1);
                    }
                }

                $urls[$key] = $url;
            }
        }

        return $urls;
    }
}
