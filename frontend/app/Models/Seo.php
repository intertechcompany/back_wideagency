<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Classes\LaravelOctoberModel;

/**
 * Class Seo
 *
 * @package App\Models
 */
class Seo extends LaravelOctoberModel
{
    /**
     * @var string
     */
    public $backendModel = 'Perevorot\Seo\Models\Seo';

    /**
     * @var string
     */
    public $table = 'perevorot_seo_seo';
}
