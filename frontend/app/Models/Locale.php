<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Classes\LaravelOctoberModel;

/**
 * Class Locale
 *
 * @package App\Models
 */
class Locale extends LaravelOctoberModel
{
    use \ModelTrait;

    /**
     * @var string
     */
    public $table = 'rainlab_translate_locales';
    /**
     * @var string
     */
    public $backendModel = 'RainLab\Translate\Models\Locale';

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeEnabled($query)
    {
        return $query->where($this->table . '.is_enabled', true);
    }
}
