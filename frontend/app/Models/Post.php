<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Classes\LaravelOctoberModel;

/**
 * Class Post
 *
 * @package App\Models
 */
class Post extends LaravelOctoberModel
{
    use \TranslatableTrait;
    use \ModelTrait;

    public const ON_HOMEPAGE = 4;

    public const PER_PAGE = 6;

    /**
     * @var string
     */
    public $table = 'intertech_wideagensy_posts';

    /**
     * @var string
     */
    public $backendModel = \Intertech\Wideagensy\Models\Post::class;

    /**
     * @var array
     */
    public $translatable = [
        'title',
        'short_description',
        'label_description_1',
        'description_1',
        'description_1',
        'label_description_2',
        'description_2',
        'quote',
        'author',
        'description_3',
        'label_description_4',
        'description_4',
        'description_5',
        'slider',
    ];

    /**
     * @var array
     */
    public $attachments = [
        'poster',
        'image_1',
        'image_2',
        'image_3',
        'image_4',
        'image_5',
    ];

    /**
     * @return mixed
     */
    public function next() {
        return Post::where([['id', '>', $this->id], ['is_access_on_url', '=', true], ['visible', '=', true]])->orderBy('id','asc')->first();
    }

    /**
     * @return mixed
     */
    public function previous() {
        return Post::where([['id', '<', $this->id], ['is_access_on_url', '=', true], ['visible', '=', true]])->orderBy('id','desc')->first();
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeEnabled($query) {
        return $query->where($this->table.'.visible', true);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeAccessInUrl($query) {
        return $query->where($this->table.'.is_access_on_url', true);
    }
}
