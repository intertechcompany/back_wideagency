<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Models\SystemSetting;

/**
 * Class Settings
 *
 * @package App\Models
 */
class Settings extends SystemSetting
{
    /**
     * @var string
     */
    public $instance = 'common';

    /**
     * @var string
     */
    public $backendModel = 'Perevorot\Settings\Models\Common';
}