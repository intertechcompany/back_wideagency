<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Classes\LaravelOctoberModel;

/**
 * Class Tour
 *
 * @package App\Models
 */
class Tour extends LaravelOctoberModel
{
    use \TranslatableTrait;
    use \ModelTrait;

    public const ON_HOME_PAGE = 4;
    public const ON_TOURS_PAGE = 5;
    public const ON_OUR_TOURS_PAGE = 7;
    public const ON_POST_PAGE = 4;

    /**
     * @var string
     */
    public $table = 'intertech_wideagensy_tours';
    /**
     * @var string
     */
    public $backendModel = \Intertech\Wideagensy\Models\Tour::class;

    /**
     * @var string[]
     */
    public $translatable = [
        'title',
        'description',
        'tour_date',
        'tour_description',
        'tour_remark',
        'member_team',
        'register_banner',
        'main_banner_description',
        'content',
        'content_second',
    ];

    /**
     * @var string[]
     */
    public $attachments = [
        'main_banner',
        'member_team_image',
    ];

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeEnabled($query) {
        return $query->where($this->table.'.visible', true);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeAccessInUrl($query) {
        return $query->where($this->table.'.is_access_on_url', true);
    }
}
