<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Classes\LaravelOctoberModel;

/**
 * Class Redirect
 *
 * @package App\Models
 */
class Redirect extends LaravelOctoberModel
{
    use \ModelTrait;

    /**
     * @var string
     */
    public $table = 'perevorot_seo_redirects';
    /**
     * @var string
     */
    public $backendModel = 'Intertech\Seo\Models\Redirect';
}
