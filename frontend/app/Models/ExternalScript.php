<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Classes\LaravelOctoberModel;

/**
 * Class ExternalScript
 *
 * @package App\Models
 */
class ExternalScript extends LaravelOctoberModel
{
    use \ModelTrait;

    /**
     * @var string
     */
    public $table = 'perevorot_seo_external';
    /**
     * @var string
     */
    public $backendModel = 'Intertech\Seo\Models\External';
}
