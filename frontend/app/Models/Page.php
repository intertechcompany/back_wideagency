<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Models\SystemSetting;

/**
 * Class Page
 *
 * @package App\Models
 */
class Page extends SystemSetting
{
    /**
     * @var string
     */
    public $instance='intertech.wideagensy.pages';

    /**
     * @var string
     */
    public $backendModel = 'Intertech\Wideagensy\Models\Page';

    /**
     * @var array
     */
    public $translatable = [
        'home_banner_date',
        'home_banner_tour_name',
        'home_banner_tour_description',
        'home_banner_btn_label',

        'home_description_block_name',
        'home_description_block_description',
        'home_description_block_description_1',
        'home_description_hashtag_label',
        'home_description_hashtag_link',

        'home_services',
        'home_about',
        'home_footer',

        'tour_banner_description',
        'tour_banner_btn_label',

        'footer_address',
        'footer_social',

        'home_footer_insta_label',
        'home_footer_insta_name',
    ];

    /**
     * @var array
     */
    public $attachments = [
        'home_banner',
        'home_description_image',
        'home_description_full_image',

        'tour_banner',
    ];
}
