<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Classes\LaravelOctoberModel;

/**
 * Class SampleModel
 *
 * @package App\Models
 */
class SampleModel extends LaravelOctoberModel
{
    use \TranslatableTrait;
    use \LongreadTrait;
    use \ModelTrait;

    /**
     * @var string
     */
    public $table = 'intertech_...';
    /**
     * @var string
     */
    public $backendModel='Intertech\...';
    /**
     * @var
     */
    public $similar;

    /**
     * @var array
     */
    protected $translatable=[
        ['name', 'primary'=>true],
        'solution'
    ];

    /**
     * @var string[]
     */
    public $attachments=[
        'image',
        'images'
    ];

    /**
     * @var string[]
     */
    protected $longread=[
        'longread'
    ];
}
