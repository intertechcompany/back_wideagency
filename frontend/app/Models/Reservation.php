<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Classes\LaravelOctoberModel;

/**
 * Class Reservation
 *
 * @package App\Models
 */
class Reservation extends LaravelOctoberModel
{
    use \TranslatableTrait;
    use \ModelTrait;

    /**
     * @var string
     */
    public $table = 'intertech_wideagensy_reservations';
    /**
     * @var string
     */
    public $backendModel = \Intertech\Wideagensy\Models\Reservation::class;

    /**
     * @var string[]
     */
    public $translatable = [
        'title',
        'description',
        'tour_description',
        'tour_remark',
        'member_team',
        'register_banner',
        'main_banner_description',
        'content',
        'content_second',
    ];

    /**
     * @var string[]
     */
    public $attachments = [
        'main_banner',
        'member_team_image',
    ];

    public const QUANTITY_ONE = 'one';
    public const QUANTITY_COUPLE = 'couple';
    public const QUANTITY_UNKNOWN = 'unknown';
}
