<?php

namespace App\Traits;

use App\Models\ExternalScript;
use App\Models\Seo;

trait SeoTrait
{
    /**
     * @param $baseUrl
     * @param $type
     *
     * @return int
     */
    public static function findByUrlMask($baseUrl, $type)
    {
        $items = [];
        $urls = self::getUrlMasks($baseUrl);

        switch(strtolower($type)) {
            case 'seo':
                $object = Seo::where([['url_mask', '=', $baseUrl], ['is_active', '=', true]]);
                break;
            case 'script':
                $object = ExternalScript::where([['url_mask', '=', $baseUrl], ['is_active', '=', true]]);
                break;
            default:
                $object = null;
                break;
        }


        foreach ($urls as $url) {
            $object = $object->orWhere('url_mask', $url);
        }

        $object = $object->get();

        foreach ($object as $item) {
            $items[substr_count($item->url_mask, '*')] = $item;
        }

        if (sizeof($items) > 0) {
            $minKey = min(array_keys($items));

            return $items[$minKey];
        }

       return null;
    }


    /**
     * @param $url
     * @return array
     */
    public static function getUrlMasks($url)
    {
        $urls = [];
        $i = sizeof(explode('/', $url));

        while ($i > 0) {
            $url = explode('/', $url);
            $url[$i - 1] = '*';
            $url = implode('/', $url);

            $urls[] = $url;

            $i--;
        }

        return $urls;
    }
}
