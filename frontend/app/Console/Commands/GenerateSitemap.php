<?php

namespace App\Console\Commands;

use App;
use App\Models\Post;
use App\Models\Tour;
use App\Services\Helpers\SeoHelper;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use LaravelLocalization;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Illuminate\Console\Command;
use App\Services\Helpers\BackendLocales;

/**
 * Class GenerateSitemap
 *
 * @package App\Console\Commands
 */
class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Sitemap
     */
    private $sitemap;

    /**
     * @var string
     */
    private $defaultLocale = 'ua';

    /**
     * @var array|null
     */
    private $locales;

    /**
     * @var string
     */
    private $path;

    /**
     * @var SeoHelper
     */
    private $seoHelper;

    /**
     * Create a new command instance.
     *
     * @param SeoHelper $seoHelper
     */
    public function __construct(SeoHelper $seoHelper)
    {
        parent::__construct();

        $this->sitemap = Sitemap::create();
        $this->locales = $this->getLocales();
        $this->defaultLocale = BackendLocales::factory()->getDefaultLocale();
        $this->path = base_path('public/sitemap.xml');

        $this->seoHelper = $seoHelper;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->formedSitemap();
            Log::info('Finish sitemap generate!');
            $this->info('Finish sitemap generate!');
        } catch(Exception $exception) {
            Log::error($exception->getMessage());
            $this->error($exception->getMessage());
        }
    }

    public function formedSitemap(): void
    {
        $this->eachCategories();
        $this->eachPages();
        $this->eachPosts();
        $this->eachTours();

        $this->sitemap->writeToFile($this->path);
    }

    private function eachCategories(): void
    {
        $this->addToSitemap($this->generateUrl('posts'), 0.8);
        $this->addToSitemap($this->generateUrl('tours'), 0.8);
    }

    private function eachPages(): void
    {
        $this->addToSitemap($this->generateUrl('home.page'), 1);
    }

    private function eachPosts(): void
    {
        $posts = Post::accessInUrl()->select('slug', 'updated_at')->get();

        foreach($posts as $post) {
            $this->addToSitemap($this->generateUrl('posts.page', ['slug' => $post->slug]), 0.5, $post->updated_at);
        }
    }

    private function eachTours(): void
    {
        $tours = Tour::accessInUrl()->select('slug', 'updated_at')->get();

        foreach($tours as $tour) {
            $this->addToSitemap($this->generateUrl('tours.page', ['slug' => $tour->slug]), 0.5, $tour->updatet_at);
        }
    }

    /**
     * @param $url
     * @param int $priority
     * @param null $lastModification
     * @param string $changeFrequency
     */
    private function addToSitemap($url, $priority = 1, $lastModification = null, $changeFrequency = Url::CHANGE_FREQUENCY_YEARLY): void
    {
        if(null !== $url) {
            $lastModification = $lastModification ?? Carbon::yesterday();

            $this->sitemap->add($url
                ->setLastModificationDate($lastModification)
                ->setChangeFrequency($changeFrequency)
                ->setPriority($priority)
            );
        }
    }

    /**
     * @param string $route
     * @param array $options
     *
     * @return Url
     */
    private function generateUrl(string $route, $options = []): ?Url
    {
        LaravelLocalization::setLocale($this->defaultLocale);
        $url = Url::create(str_replace(url('/'), url('/'), route($route, $options)));

        if($this->checkIndexing($route, $url->url)) {
            foreach($this->locales as $locale) {
                LaravelLocalization::setLocale($locale['code']);
                $alternateUrl = str_replace(url('/'), url('/') . '/' . $locale['code'], route($route, $options));

                if($this->checkIndexing($route, str_replace(url('/'), '', $alternateUrl))) {
                    $url->addAlternate(str_replace(url('/'), url('/') . '/' . $locale['code'], route($route, $options)), $locale['code']);
                }
            }

            return $url;
        }

        return null;
    }

    /**
     * @param string $route
     * @param string $path
     *
     * @return bool
     */
    private function checkIndexing(string $route, string $path)
    {
        $seo = $this->seoHelper->getSeo($route, str_replace(url('/') . '/', '', $path));

        if(null !== $seo && $seo->is_active && !$seo->is_indexing) {
            return false;
        }

        return true;
    }

    /**
     * @return array|null
     */
    private function getLocales(): ?array
    {
        $locales = BackendLocales::factory()->getSupportedLocales();

        if(isset($locales[$this->defaultLocale])) {
            unset($locales[$this->defaultLocale]);
        }

        return $locales;
    }
}

