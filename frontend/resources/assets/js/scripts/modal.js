const getTemplate = (label, description, btn) => {
    return`
        <div class="modal__backdrop" data-type="backdrop">
            <div class="modal__content">
                <a class="modal__close" data-type="close"></a>
    
                <span class="modal__label">${label}</span>
                <span class="modal__description">${description}</span>
    
                <span class="logo"></span>
    
                <div class="modal__btn" data-type="close">
                    <span data-type="close">${btn}</span>
                </div>
            </div>
        </div>
    `
};

export class Modal {
    constructor(selector, options) {
        this.$el = document.querySelector(selector);
        if(null !== this.$el) {
            this.render();
            this.setup();
        }
    }

    render() {
        this.$el.classList.add('modal');
        const {label, description, button} = this.$el.dataset;
        this.$el.innerHTML = getTemplate(label || 'Info', description || '', button || 'Close');
    }

    setup() {
        this.clickHandler = this.clickHandler.bind(this);

        this.$el.addEventListener('click', this.clickHandler);
    }

    clickHandler(event) {
        const {type} = event.target.dataset;

        if('backdrop' === type || 'close' === type) {
            this.close()
        }
    }

    get isOpen() {
        return this.$el.classList.contains('open')
    }

    toggle() {
        this.isOpen ? this.close() : this.open()
    }

    open() {
        if(!this.$el.classList.contains('open')) {
            this.$el.classList.add('open');
        }

        if(!document.body.classList.contains('scroll__disable')) {
            document.body.classList.add('scroll__disable');
        }
    }

    close() {
        if(this.$el.classList.contains('open')) {
            this.$el.classList.remove('open');
        }

        if(document.body.classList.contains('scroll__disable')) {
            document.body.classList.remove('scroll__disable');
        }
    }

    destroy() {
        this.$el.removeEventListener('click', this.clickHandler);
        this.$el.innerHTML = ''
    }
}