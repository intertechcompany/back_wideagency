export function setErrors(errors, dataValidation) {
    if(errors) {
        let i = 0;
        for (let [key, value] of Object.entries(errors)) {
            let $element = document.querySelector(`[data-validation="${dataValidation}-${key}"]`);
            if(null !== $element) {
                if(0 === i) {
                    $element.focus();
                }

                $element.classList.add('validation-error');
                // $element.insertAdjacentHTML('beforebegin', `<p class='error-message'>${value}</p>`);

                i++;
            }
        }
    }
}

export function clearErrors() {
    [].forEach.call(document.querySelectorAll('.validation-error'),function(e) {
        e.classList.remove('validation-error');
    });

    // [].forEach.call(document.querySelectorAll('.error-message'),function(e) {
    //     e.parentNode.removeChild(e);
    // });
}
