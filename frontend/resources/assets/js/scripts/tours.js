
$(document).ready(function () {

	$('.partner__list').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		infinite: false,
		draggable: false,
		arrows: true,
		prevArrow: "<button type='button' class='slick-prev team-slider-prev'>←</button>",
		nextArrow: "<button type='button' class='slick-next team-slider-next'>→</button>",
		responsive: [{
			breakpoint: 992,
			settings: "unslick"
		}]
	});

	// Travel show more

	if ($(window).width() < 992) {

		if ($('.travels__item').length > 4) {
			$('.travels__item:gt(3)').hide();
			$('.show-more-btn').fadeIn();
		}
	
		$('.show-more-btn').on('click', function () {
			
			$('.travels__item:gt(3)').slideToggle();
			//change text of show more element just for demonstration purposes to this demo
			// $(this).text() === '→ показати більше' ? $(this).text('→ показати меньше') : $(this).text('→ показати більше');
			$(this).fadeOut();
		});
	} else {
		if ($('.travels__item').length > 7) {
			$('.travels__item:gt(6)').hide();
			$('.show-more-btn').fadeIn();
		}
	
		$('.show-more-btn').on('click', function () {
			$('.travels__item:gt(6)').slideToggle();
			$(this).fadeOut();
		});
	}

	// Last travel show more


	if ($(window).width() < 992) {

		if ($('.last__item').length > 2) {
			$('.last__item:gt(1)').hide();
			$('.last__link').fadeIn();
		}
	
		$('.last__link').on('click', function () {
			
			$('.last__item:gt(4)').slideToggle();
			//change text of show more element just for demonstration purposes to this demo
	
			$(this).fadeOut();
			// $(this).text() === '→ показати більше' ? $(this).text('→ показати меньше') : $(this).text('→ показати більше');
		});
	} else {
	
		if ($('.last__item').length > 5) {
			$('.last__item:gt(4)').hide();
			$('.last__link').fadeIn();
		}
	
		$('.last__link').on('click', function () {
			
			$('.last__item:gt(4)').slideToggle();
			//change text of show more element just for demonstration purposes to this demo
	
			$(this).fadeOut();
			// $(this).text() === '→ показати більше' ? $(this).text('→ показати меньше') : $(this).text('→ показати більше');
		});
	}
});




