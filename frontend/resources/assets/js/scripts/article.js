'use strict';

$(document).ready(function () {

    var $status = $('.article-gallery__count');
    var $slickElement = $('.article-gallery');

    $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        if (!slick.$dots) {
            return;
        }

        var i = (currentSlide ? currentSlide : 0) + 1;
        var a = slick.$dots[0].children.length;
        $status.text('0' + i + ' / ' + ('0' + slick.$dots[0].children.length));

        if (i > 9) {
            $status.text('' + i + ' / ' + ('' + slick.$dots[0].children.length));
        }
        if (a > 9) {
            $status.text('' + i + ' / ' + ('' + slick.$dots[0].children.length));
        }
    });

    $('.article-gallery').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        dots: true,
        arrows: true,
        prevArrow: "<button type='button' class='slick-prev'><span>←</span></button>",
        nextArrow: "<button type='button' class='slick-next'><span>→</span></button>",
        // responsive: [{
        //     breakpoint: 1200,
        //     settings: {
        //         prevArrow: "<button type='button' class='slick-prev'>←</button>",
        //         nextArrow: "<button type='button' class='slick-next'>→</button>",
        //         infinite: false
        //     }
        // }, ]
    });


});