$(document).ready(function () {

	function desktopOnlySlider() {
		$('.team__list').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: false,
			draggable: false,
			arrows: true,
			prevArrow: "<button type='button' class='slick-prev team-slider-prev'>←</button>",
			nextArrow: "<button type='button' class='slick-next team-slider-next'>→</button>",
			responsive: [{
				breakpoint: 992,
				settings: "unslick"
			}]
		});
		
	}

	desktopOnlySlider();

	$(window).resize(function(e){
		if(window.innerWidth > 992) {
			if(!$('.team__list').hasClass('slick-initialized')){
				desktopOnlySlider();
			}
	
		} else{
			if($('.team__list').hasClass('slick-initialized')){
				$('.team__list').slick('unslick');
			}
		}
	});

	
	// $(function () {
	// 	$(window).scroll(function () {

	// 		let top = $(document).scrollTop();
	// 		let footer = $('.footer').offset().top;

	// 		if (top > 735) $('.hashtag').addClass('hashtag_fixed');

	// 		else {
	// 			$('.hashtag').removeClass('hashtag_fixed');
	// 			$('.hashtag').removeClass('hashtag_fixed-bottom');
	// 		} 

	// 		if (top > footer - 330 ) {
	// 			// $('.hashtag').addClass('hashtag_fixed-bottom');
	// 			$('.hashtag').removeClass('hashtag_fixed');
	// 		} 
	// 	});
	// });
});
