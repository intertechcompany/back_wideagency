
 export function showMobileMenu() {
     const $hamburger = document.querySelector('.hamburger');
     const $headerNav = document.querySelector('.header__nav');

     if (null !== $hamburger && null !== $headerNav) {
         $hamburger.classList.toggle('hamburger_active');
         $headerNav.classList.toggle('header__nav_active');
     }
 }

