export function showNotification(message) {
    $notyf.open({
        type: 'info',
        message: message,
    });
}

export function showNotificationWithTranslate(defaultMessage, translateMessage, translateCode) {
    if(null !== translateMessage && typeof translateMessage !== 'undefined' && translateMessage !== translateCode) {
        defaultMessage = translateMessage;
    }

    showNotification(defaultMessage);
}
