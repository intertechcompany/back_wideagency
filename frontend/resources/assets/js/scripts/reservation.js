import {clearErrors, setErrors} from "../scripts/validation";
// import {showNotificationWithTranslate} from "../scripts/notyf";
import {Modal} from "./modal";

export class Reservation {
    constructor(formName, submitBtnId) {
        this.formName = formName;
        this.$btn = document.getElementById(submitBtnId);

        this.$form = document.forms[this.formName];

        this.$reservationModal = new Modal('#reservationModal-js');

        this.setup()
    }

    setup() {
        this.clickHandler = this.clickHandler.bind(this);

        if(null !== this.$btn) {
            this.$btn.addEventListener('click', this.clickHandler)
        }
    }

    clickHandler(event) {
        event.preventDefault();

        const {link, notyf} =  event.target.dataset;

        this.send(link, notyf);
    }

    send(link, notyf) {
        const data = {
            'name': this.$form.name.value,
            'email': this.$form.email.value,
            'phone': this.$form.phone.value,
            'comment': this.$form.comment.value,
            'quantity': this.$form.quantity.dataset.value,
            'agreement': this.$form.agreement.checked,
        };

        $axios.post(link, data)
            .then((response) => {
                if (response.data.success) {
                    clearErrors();
                    document.forms[this.formName].reset();

                    this.$reservationModal.open();
                    // showNotificationWithTranslate('Reserved', notyf, 'registration.labels.notyf.registration');
                } else {
                    clearErrors();
                    setErrors(response.data.errors, 'reservation');
                }
            })
            .catch((error) => {
                console.log(error);
            })
        ;
    }
}