
// let prevScrollpos = window.pageYOffset;
// const $header = document.getElementById('header-wrapper');
// const $headerNav = document.querySelector('.header__nav');
// console.log($header)
// console.log($headerNav)

// if(null !== $header && null !== $headerNav) {
//   window.addEventListener('scroll', () => {
//     let currentScrollPos = window.pageYOffset;

//     if (prevScrollpos > currentScrollPos) {
//       $header.style.opacity = '1';
//     } else {
//       $header.style.opacity = '0';
//       $headerNav.classList.remove('header__nav_active');
//     }
//     prevScrollpos = currentScrollPos;
//   });
// }

var header = $('#header-wrapper'),
    nav = $('#header__nav'),
scrollPrev = 0;

$(window).scroll(function() {
  var scrolled = $(window).scrollTop();
 
  if ( scrolled > 0 && scrolled > scrollPrev ) {
    header.css('transform', 'translateY(-100%)');
  } else {
    header.css('transform', 'translateY(0)');
    nav.removeClass('header__nav_active');
  }
  scrollPrev = scrolled;
});