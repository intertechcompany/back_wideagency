require('./scripts/fix-menu');

import axios from 'axios';
import {Notyf} from "notyf";
import {Reservation} from './scripts/reservation';
import {showMobileMenu} from './scripts/header';

import 'notyf/notyf.min.css';

axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
};

window.$axios = axios;
window.$notyf = new Notyf({
    types: [
        {
            type: 'info',
            background: '#222222',
            icon: false
        }
    ],
    duration: 5000,
    position: {
        x:'right',
        y:'top',
    },
    dismissible: true,
});


let $menu = document.querySelector(".menu-btn");
if (null !== $menu) {
    $menu.addEventListener("click", showMobileMenu, false);
}

const reservation = new Reservation('reservation', 'reservation-btn-js');

const WOW = require('wow.js');

window.wow = new WOW();
window.wow.init();