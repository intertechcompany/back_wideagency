@php
    $footer = App\Models\Page::instance();
@endphp

@if(!Route::is('tours.page'))
    <!-----------------------------Section insta------------------>
    @if(isset($footer->home_footer[0]->home_footer_image) && !empty($footer->home_footer[0]->home_footer_image))
        <section class="insta">
            <h4 class="insta__title wow animate__animated animate__fadeInUp">{{ $footer->home_footer_insta_label }} <br><a href="{{ $footer->home_footer_insta_link }}"><span>→</span> {{ $footer->home_footer_insta_name }}</a></h4>
            <ul class="insta__list">
                @foreach(array_chunk($footer->home_footer, 2) as $inst)
                    <div class="insta__item-wrap wow animate__animated animate__fadeInUp">
                    @foreach($inst as $key => $instItem)
                        <!----------------Insta item------------>
                            <a href="{{ $instItem->home_footer_link ?? '' }}" class="insta__item">
                                <img src="{{  null === $instItem->home_footer_image ? '' : env('BACKEND_CORS_ORIGIN_URL') . env('STORAGE_URL') . '/media'. $instItem->home_footer_image }}" alt="{{$instItem->home_footer_image_alt ?? ''}}">
                            </a>
                        @endforeach
                    </div>
                @endforeach
            </ul>
        </section>
    @endif
@endif