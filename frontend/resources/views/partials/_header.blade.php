@php
    $header = App\Models\Page::instance();
@endphp
<div class="header-wrapper" id="header-wrapper">
    <header class="header"
{{--        @if(!$header->is_default_header_color && !empty($header->header_color))--}}
{{--            style="background-color: {{ $header->header_color }}"--}}
{{--        @endif--}}
>
   <div class="header__logo">
       <a href="{{ localized_url(route('home.page')) }}">
           <img src="/img/logo.svg" alt="Logo">
       </a>
   </div>


    <!--------------------Header nav------------->
    <nav class="header__nav">
        <ul class="header__nav-list">
            <li class="header__nav-item">
                <a href="{{ localized_url(route('tours')) }}" class="header__nav-link">{{ translate('header.nav.labels.travel') }}</a>
            </li>
            <li class="header__nav-item">
                <a href="{{ localized_url(route('posts')) }}" class="header__nav-link">{{ translate('header.nav.labels.stories') }}</a>
            </li>
        </ul>
        <div>

            @foreach (LaravelLocalization::getSupportedLocales() as $locale => $lang)
                @if (LaravelLocalization::getCurrentLocale() === $locale)
                    <a href="#" class="header__lang header__lang_active">{{ translate('header.nav.lang.btn.' .$locale) }}</a>
                @else
                    <a hreflang="{{ $locale }}" href="{{ LaravelLocalization::getLocalizedURL($locale, null, [], false) }}" class="header__lang">{{ translate('header.nav.lang.btn.' .$locale) }}</a>
                @endif
            @endforeach
        </div>
    </nav>

    <div class="menu-btn" id="mobileMenu">
        <div class="hamburger">
            <span></span>
            <span></span>
        </div>
    </div>
</header>
</div>