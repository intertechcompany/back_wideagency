@php
    $footer = App\Models\Page::instance();
@endphp

@if(!isset($exception) && !Route::is('home.page'))
    @include('partials._insta')
@endif

<footer class="footer"
        @if(!$footer->is_default_footer_color && !empty($footer->footer_color))
            style="background-color: {{ $footer->footer_color }}"
        @endif
        >
    <div class="footer__container">
        <h4 class="footer__title  wow animate__animated animate__fadeInUp">{{ translate('footer.labels.all-travel-in') }} <span>→</span> <a href="{{ translate('footer.links.telegram') }}">{{ translate('footer.labels.telegram') }}</a></h4>

        <!----------------Footer contact----------------->
        <div class="footer__contact  wow animate__animated animate__fadeInUp">
            <div class="footer__tel-wrap">
                <a href="tel:{{ $footer->footer_phone }}" class="footer__contact-tel">{{ $footer->footer_phone }}</a>
                <a href="mailto:{{ $footer->footer_email }}" class="footer__contact-mail">{{ $footer->footer_email }}</a>
            </div>

            @if(isset($footer->footer_social[0]->footer_social_link) && !empty($footer->footer_social[0]->footer_social_label))
                <div class="footer__social">
                    @foreach(array_chunk($footer->footer_social, 2) as $key => $items)
                        <div class="footer__social-item-wrap">
                            @foreach($items as $k => $item)
                                <a href="{{ $item->footer_social_link ?? '' }}"
                                   class="
                                    footer__social-item
                                    @if(0 === $key && 0 === $k)
                                      footer__social-line
                                    @endif
                                    "
                                >{{ $item->footer_social_label ?? '' }}</a>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            @endif

            <address class="footer__address">{{ translate('footer.labels.shop.goodwine') }} <span>{{ $footer->footer_address }}</span></address>
        </div>
        <!--------------------Footer bottom---------- -->
        <div class="footer__bottom  wow animate__animated animate__fadeInUp">
            <div class="footer__copy">&copy; {{ date('Y') }}</div>
            <div class="footer__bottom-partner">{{ translate('footer.labels.partner') }}</div>
            <a href="#" class="footer__bottom-web">{{ translate('footer.labels.website') }} <span>{{ translate('footer.labels.website.name') }}</span></a>
        </div>
    </div>
</footer>
