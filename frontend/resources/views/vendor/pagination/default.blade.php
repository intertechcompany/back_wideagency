@if ($paginator->hasPages())
    <div class="blog__pag wow animate__animated animate__fadeInU">

        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <button type="button" class="blog__pag-btn">←</button>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"><button type="button" class="blog__pag-btn">←</button></a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="blog__pag-item blog__pag_active" aria-current="page">{{ $page }}</li>
                    @else
                        <li class="blog__pag-item" aria-current="page"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><button type="button" class="blog__pag-btn">→</button></a>
        @else
            <button type="button" class="blog__pag-btn">→</button>
        @endif

    </div>
@endif
