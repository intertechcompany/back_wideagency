@extends('layouts.app')

@section('style')
    <link href="{{ mix('css/main-page.css') }}" rel="stylesheet">
@endsection

@section('content')

    @if($data['success'])
        <!-----------------Main Banner------------------->
        @if(isset($data['data']['settings']))
            <section class="banner">
                <div
                        class="banner__info"
                        @if(!$data['data']['settings']->is_default_home_banner_color && !empty($data['data']['settings']->home_banner_color))
                        style="background-color: {{ $data['data']['settings']->home_banner_color }}"
                        @endif
                >
                    <h1 class="banner__title animate__animated animate__fadeInUp-2"><span class="italic"><span
                                    class="icon">→</span> {{ $data['data']['settings']->home_banner_date ?? '' }}</span> {{ $data['data']['settings']->home_banner_tour_name ?? '' }}
                    </h1>
                    <div class="banner__descr animate__animated animate__fadeInUp-2">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['settings']->home_banner_tour_description ?? '' ) !!}
                    </div>
                    <a href="{{ $data['data']['settings']->home_banner_btn_link ?? '' }}"
                       class="banner__link animate__animated animate__fadeInUp-2"> {{ $data['data']['settings']->home_banner_btn_label ?? '' }}</a>
                </div>
                <div class="banner__img">
                    <a href="{{ $data['data']['settings']->home_banner_btn_link ?? '' }}">
                    <img src="{{ null === $data['data']['settings']->home_banner ? '' : env('BACKEND_CORS_ORIGIN_URL') . $data['data']['settings']->home_banner->path }}"
                         alt="{{  $data['data']['settings']->home_banner->title ?? 'Banner image' }}">
                    </a>
                </div>

                <!-----------------Animation legs---------->
                <div class="animation-legs animate__animated animate__fadeInUp-legs">
                    <img src="/img/a2.svg" alt="Animation leg" class="left-leg">
                    <img src="/img/a2.svg" alt="Animation leg" class="right-leg">
                </div>
            </section>

            <div class="main-wrap">
                <div class="main">
                    <!-------------------History------------------->
                    <section class="history">
                        <h2 class="history__title wow animate__animated animate__fadeInUp">
                            {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['settings']->home_description_block_name ?? '' ) !!}
                        </h2>
                        <div class="history__descr wow animate__animated animate__fadeInUp">
                            <div class="history__descr-item">
                                {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['settings']->home_description_block_description ?? '' ) !!}
                            </div>
                            <div class="history__descr-item">
                                {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['settings']->home_description_block_description_1 ?? '') !!}
                            </div>
                        </div>
                    </section>

                    <!-------------------Travels------------------->
                    @if(isset($data['data']['tours']) && !$data['data']['tours']->isEmpty())
                        <section class="travels">
                            <h2 class="travels__title wow animate__animated animate__fadeInUp">{{ translate('home.labels.future-travel') }}</h2>
                            <ul class="travels__list">
                                <!-------------Travel item----------------->
                                @foreach($data['data']['tours'] as $tour)
                                    <li class="travels__item wow animate__animated animate__fadeInUp">
                                        <a href="{{ localized_url(route('tours.page', ['slug' => $tour->slug])) }}">
                                            <div class="travels__item-img-wrap">
                                            <img src="{{ null === $tour->main_banner ? '' : env('BACKEND_CORS_ORIGIN_URL') . $tour->main_banner->path }}"
                                                 alt="{{ $tour->main_banner->title ?? 'Travel Image'}}" class="travels__item-img">
                                            </div>
                                            <div class="travels__item-descr">
                                                <h3 class="travels__item-title"><span class="underline">{{ $tour->title }}</span></h3>
                                                <div class="travels__item-date">
                                                    wine tour<span class="icon">→</span><span
                                                            class="italic">{{ $tour->tour_date }}</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="travels__link-wrap wow animate__animated animate__fadeInUp">
                            <a href="{{ localized_url(route('tours')) }}"
                               class="travels__link
                                @if(0 !== $data['data']['tours']->count() % 2)
                                       travels__link-position
                                @endif
                                       "><span>→</span>{{ translate('home.btn.view-another-tours') }}</a>
                            </div>
                        </section>
                    @endif

                    <!-----------------------------------Section offer--------------------------------------->
                    @if(isset($data['data']['settings']->home_services[0]->home_services_description) && !empty($data['data']['settings']->home_services[0]->home_services_description))
                        <section class="offer">
                            <h2 class="offer__title wow animate__animated animate__fadeInUp">{{ translate('home.labels.offer') }} <span
                                        class="italic"> {{ translate('home.labels.offer-em') }}</span></h2>
                            <!-------------------Offer list-------------->
                            <ol class="offer__list">
                                @foreach($data['data']['settings']->home_services as $service)
                                    <li class="offer__item wow animate__animated animate__fadeInUp">{{ $service->home_services_description }}</li>
                                @endforeach
                            </ol>
                        </section>
                    @endif


                    <!-----------------------------Team section---------------------->
                    <section class="team">
                        <h3 class="team__title wow animate__animated animate__fadeInUp">{{ translate('home.labels.team') }}</h3>

                        <div class="team__list-wrapper wow animate__animated animate__fadeInUp">
                            <!------------Team list------------------>
                            <ul class="team__list">
                            @if(isset($data['data']['settings']->home_about[0]->home_about_name) && !empty($data['data']['settings']->home_about[0]->home_about_name))
                                <!-----------Team item------->
                                    @foreach($data['data']['settings']->home_about as $person)
                                        <li class="team__item">
                                            <img src="{{ null === $person->home_about_image ? '' : env('BACKEND_CORS_ORIGIN_URL') . env('STORAGE_URL') . '/media'.  $person->home_about_image }}"
                                                 alt="{{  $person->home_about_image_alt ?? '' }}" class="team__item-img">
                                            <div class="team__item-info">
                                                <div class="team__item-name">{{ $person->home_about_name }}</div>
                                                <div class="team__item-post">{{ $person->home_about_position }}</div>
                                                <div class="team__item-descr">
                                                    {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $person->home_about_description ?? '') !!}
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </section>

                    <!---------------------Section blog----------------------->
                    @if(isset($data['data']['posts']) && !$data['data']['posts']->isEmpty())
                        <section class="blog">
                            <h2 class="blog__title wow animate__animated animate__fadeInUp">{{ translate('home.labels.blog') }}</h2>
                            <ul class="blog__list">

                            @foreach($data['data']['posts'] as $post)
                                <!--------------Blog item--------------->
                                    <li class="wow animate__animated animate__fadeInUp">
                                        <a href="{{ localized_url(route('posts.page', ['slug' => $post->slug])) }}"
                                           class="blog__item">
                                            <div class="blog__img-wrap">
                                            <img src="{{ null === $post->poster ? '' : env('BACKEND_CORS_ORIGIN_URL') . $post->poster->path }}"
                                                 class="blog__img" alt="{{ $post->poster->title ?? '' }}">
                                            </div>
                                            <div class="blog__item-info">
                                                <div class="blog__item-date">{{ \App\Services\Helpers\DataHelper::getMonth($post->created_at->format('m')) .', '. $post->created_at->format('Y') }}</div>
                                                <h3 class="blog__item-title">{{ $post->title }}</h3>
                                            </div>
                                        </a>
                                    </li>

                                @endforeach

                                <div class="blog__link-wrap">
                                    <a href="{{ localized_url(route('posts')) }}"
                                       class="blog__link"><span>→</span> {{ translate('home.btn.more-posts') }}</a>
                                </div>
                            </ul>
                        </section>
                    @endif

                    @include('partials._insta')
                </div>

                <!---------------------Hashtag--------------------->
                    <div class="hashtag-wrapper">
                        <div class="hashtag">{{ translate('footer.labels.hashtag') }}</div>
                    </div>
            </div>
        @endif
    @endif
@endsection

@section('scripts')
    <script defer src="{{ mix('js/custom.js') }}"></script>
@endsection