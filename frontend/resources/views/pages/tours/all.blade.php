@extends('layouts.app')

@section('style')
    <link href="{{ mix('css/tours.css') }}" rel="stylesheet">
@endsection

@section('content')
    @if($data['success'])
        @if(isset($data['data']['tours']) && !$data['data']['tours']->isEmpty())
             <!-------------------Travels------------------->
             <section class="travels travels_pad">
            <h1 class="travels__title travels__title_size animate__animated animate__fadeInUp">{{ translate('tours.labels.our-tours') }}</h1>
            <ul class="travels__list">
                <!-------------Travel item----------------->
                @foreach($data['data']['tours'] as $tour)
                    <li class="travels__item animate__animated animate__fadeInUp">
                        <a href="{{ localized_url(route('tours.page', ['slug' => $tour->slug])) }}">
                            <div class="travels__item-img-wrap">
                                <img src="{{null === $tour->main_banner ? '' : env('BACKEND_CORS_ORIGIN_URL') . $tour->main_banner->path }}" alt="{{ $tour->main_banner->title ?? '' }}" class="travels__item-img">
                            </div>
                            <div class="travels__item-descr">
                                <h3 class="travels__item-title"><span class="underline">{{ $tour->title }}</span></h3>
                                <div class="travels__item-date">
                                    wine tour<span class="icon">→</span>{{ $tour->tour_date }}
                                    <span class="travels__item-message">{!! $tour->tour_remark !!}</span>
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
                 @if($data['data']['tours_count'] > \App\Models\Tour::ON_OUR_TOURS_PAGE)
                     <div class="show-more-btn-wrap">
                         <button type="button" class="show-more-btn wow animate__animated animate__fadeInUp"><span>→</span> {{ translate('tours.btn.show.more') }}</button>
                     </div>
                 @endif
        </section>
        @endif

        <!------------------------------Write us---------------------->
        @if(isset($data['data']['settings']))
            <section class="write-us">
                <div  class="write-us__wrap">
                    <h2 class="write-us__descr wow animate__animated animate__fadeInUp">{!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media',$data['data']['settings']->tour_banner_description ?? '') !!}</h2>
                    <div href="mailto:{{ $data['data']['settings']->tour_banner_btn_link }}" class="write-us__link wow animate__animated animate__fadeInUp"><span>→ </span><a href="mailto:{{ $data['data']['settings']->tour_banner_btn_link }}">{{ $data['data']['settings']->tour_banner_btn_label }}</a></div>
                </div>
            </section>
        @endif

        @if(isset($data['data']['partner_tours']) && !$data['data']['partner_tours']->isEmpty())
            <!------------------------Section Partner Travel-------------->
            <section class="partner">
                <h2 class="partner__title wow animate__animated animate__fadeInUp">{{ translate('tours.labels.partner-tours') }}</h2>
                <div class="partner__list-wrapper wow animate__animated animate__fadeInUp">
                    <div class="partner__list">
                        <!-----------------Partner travel item------>
                        @foreach($data['data']['partner_tours'] as $partnerTour)
                            <a href="{{ localized_url(route('tours.page', ['slug' => $partnerTour->slug])) }}" class="partner__item">
                                <div class="partner__item-img-wrap">
                                    <img src="{{ null === $partnerTour->main_banner ? '' : env('BACKEND_CORS_ORIGIN_URL') . $partnerTour->main_banner->path }}" alt="{{ $partnerTour->main_banner->title ?? '' }}" class="partner__item-img">
                                </div>
                                <h3 class="partner__item-title">{{ $partnerTour->title }}</h3>
                            </a>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif

        @if(isset($data['data']['past_tours']) && !$data['data']['past_tours']->isEmpty())
            <!-----------------------------Section last travel------------------>
            <section class="last">
                <h2 class="last__title wow animate__animated animate__fadeInUp">{{ translate('tours.labels.past-tours') }}</h2>

                <ul class="last__list">

                    <!-------------Travel item----------------->
                    @foreach($data['data']['past_tours'] as $pastTour)
                        <li class="last__item wow animate__animated animate__fadeInUp">
                            <a href="{{ localized_url(route('tours.page', ['slug' => $pastTour->slug])) }}">
                                <div class="last__item-img-wrap">
                                    <img src="{{ null === $pastTour->main_banner ? '' : env('BACKEND_CORS_ORIGIN_URL') . $pastTour->main_banner->path }}" alt="{{ $pastTour->main_banner->title ?? '' }}" class="last__item-img">
                                </div>
                                    <div class="last__item-descr">
                                        <h4 class="last__item-title"><span class="underline">{{ $pastTour->title }}</span></h4>
                                    <div class="last__item-date">
                                        wine tour<span class="icon">→</span>{{ $pastTour->tour_date }}
                                    </div>
                                </div>
                            </a>
                        </li>
                    @endforeach

                    @if($data['data']['past_tours_count'] > \App\Models\Tour::ON_TOURS_PAGE)
                        <div class="last__btn-wrap" >
                            <button type="button" class="last__link"><span>→</span> {{ translate('home.btn.view-another-tours') }}</button>
                        </div>
                    @endif
                </ul>

            </section>
        @endif
    @endif
@endsection

@section('scripts')
    <script defer src="{{ mix('js/tours.js') }}"></script>
@endsection
