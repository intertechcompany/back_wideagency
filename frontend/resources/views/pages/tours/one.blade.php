@extends('layouts.app')

@section('style')
    <link href="{{ mix('css/tour.css') }}" rel="stylesheet">
@endsection

@section('content')
    @if($data['success'])
        @if(isset($data['data']['tour']))

            <!-----------------Main Banner------------------->
            <section class="banner">
                <div
                        class="banner__info banner_height"
                        @if(!$data['data']['tour']->is_default_main_banner_color && !empty($data['data']['tour']->main_banner_color))
                            style="background-color: {{ $data['data']['tour']->main_banner_color }}"
                        @endif
                >
                    <h1 class="banner__title animate__animated animate__fadeInUp-2"><span
                                class="italic">→ {{ $data['data']['tour']->tour_date }}</span> {{ $data['data']['tour']->title }}
                    </h1>
                    <div class="banner__descr animate__animated animate__fadeInUp-2">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media',$data['data']['tour']->main_banner_description ?? '') !!}
                    </div>
                </div>
                <div class="banner__img">
                    <img src="{{ null === $data['data']['tour']->main_banner ? '' : env('BACKEND_CORS_ORIGIN_URL') . $data['data']['tour']->main_banner->path }}"
                         alt="{{ $data['data']['tour']->main_banner->title ?? '' }}">
                </div>

                <!-----------------Animation legs---------->
                <div class="animation-legs animate__animated animate__fadeInUp-legs">
                    <img src="/img/a2.svg" alt="Animation leg" class="left-leg">
                    <img src="/img/a2.svg" alt="Animation leg" class="right-leg">
                </div>
            </section>

            <!---------------------Section Main--------------------->
            <section class="tour">

                <!----------------Content tour---------->
                <div class="content">
                {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['tour']->content ?? '') !!}



                <!-----------------------Reservation block visible only on mobile---------------------->
                    <div class="content__reservation-block">
                        <div class="reservation__price-wrap-mob wow animate__animated animate__fadeInUp">
                            <div class="reservation__price">{{ translate('tour.labels.price.two') }}  → <span class="italic">{{ $data['data']['tour']->tour_price_for_two }}</span></div>
                            <div class="reservation__price">{{ translate('tour.labels.price.one') }} → <span class="italic">{{ $data['data']['tour']->tour_price_for_one }}</span></div>
                        </div>

                        <!----------------Detail list-------------->
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['tour']->tour_description ?? '') !!}
                    </div>


                <!------------------------------Registration link---------------------->
                    <div class="reg-link__wrap wow animate__animated animate__fadeInUp">
                        <p class="reg-link__descr">
                            {{ $data['data']['tour']->register_banner }}
                        </p>
                        <div class="reg-link__link">
                            <span>→ </span><a href="#registration">{{ translate('tour.btn.register') }}</a>
                        </div>
                    </div>

                    {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['tour']->content_second ?? '') !!}
                </div>

                <!---------------Reservation tour----------->
                <div class="reservation-wrap wow animate__animated animate__fadeInUpMob">
                    <div class="reservation">
                        <div class="reservation__price-wrap">
                            <div class="reservation__price">{{ translate('tour.labels.price.two') }} → <span
                                        class="italic">{{ $data['data']['tour']->tour_price_for_two }}</span></div>
                            <div class="reservation__price">{{ translate('tour.labels.price.one') }} → <span
                                        class="italic">{{ $data['data']['tour']->tour_price_for_one }}</span></div>
                        </div>

                        <div class="reservation__btn-wrap">
                            <div class="reservation__places">
                                @if($data['data']['tour']->is_no_places)
                                    {{ translate('tour.labels.no-places') }}
                                @else
                                    {!! $data['data']['tour']->tour_remark !!}
                                @endif
                            </div>
                            <div class="reservation__message">
                                {{ translate('tour.labels.remark.description') }}
                            </div>

                            <a href="#registration"
                               class="reg__submit reservation__btn">
                             @if($data['data']['tour']->is_past)
                                {{ translate('tour.btn.reservation-past') }}
                             @elseif($data['data']['tour']->is_no_places)
                                {{ translate('tour.btn.reservation-no-place') }}
                             @else
                                {{ translate('tour.btn.reservation') }}
                             @endif
                            </a>
                        </div>

                        <div class="reservation__details-wrap">
                            <!----------------Detail list-------------->
                            {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['tour']->tour_description ?? '') !!}
                        </div>
                    </div>
                </div>


            </section>

            @include('pages.tours.partial._registration', [
                'is_past' => $data['data']['tour']->is_past,
                'is_no_place' => $data['data']['tour']->is_no_places,
            ])
        @endif
    @endif
@endsection

@section('scripts')
    <script defer src="{{ mix('js/tour.js') }}"></script>
@endsection