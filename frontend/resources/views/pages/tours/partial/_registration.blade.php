<!--------------------------Registration section------------------>
<section class="reg" id="registration">
    <div class="reg__info">
        <h3 class="reg__title wow animate__animated animate__fadeInUp">{{ translate('registration.labels.registration') }}</h3>
        <p class="reg__descr wow animate__animated animate__fadeInUp">{{ translate('registration.labels.description') }} <span class="italic">{{ translate('registration.labels.description-1') }}</span></p>
{{--        <div class="reg__link">--}}
{{--            <span>{{ translate('registration.labels.question') }} </span>--}}
{{--            →<a href="mailto:{{ translate('registration.links.email') }}">{{ translate('registration.links.email') }}</a>--}}
{{--        </div>--}}
    </div>

    <!--------------------------Registration form--------------->

    <form name="reservation" method="POST" class="reg__form">
        <input
                type="text"
                placeholder="{{ translate('registration.placeholders.name') }}"
                name="name"
                class="reg__input"
                data-validation="reservation-name"
        >
        <input
                type="number"
                placeholder="{{ translate('registration.placeholders.phone') }}"
                name="phone"
                class="reg__input"
                data-validation="reservation-phone"
        >
        <input
                type="email"
                placeholder="{{ translate('registration.placeholders.email') }}"
                name="email"
                class="reg__input"
                data-validation="reservation-email"
        >
        <input
                type="text"
                placeholder="{{ translate('registration.placeholders.comment') }}"
                name="comment"
                class="reg__input"
        >

        <!-- ---------------Form dropdown---------------->
        <div class="form-dropdown count-dropdown wow animate__animated animate__fadeInUp">
            <div class="dropdown-list count-dropdown-list">
                <div class="reg__input dropdown-title">
                    <input
                            class="count-input"
                            placeholder="{{ translate('registration.placeholders.quantity') }}"
                            value="{{ translate('registration.labels.quantity.not-one') }}"
                            name="quantity"
                            data-name="quantity"
                            data-value="{{ \App\Models\Reservation::QUANTITY_COUPLE }}"
                            data-validation="reservation-quantity"
                            readonly
                    >
                    <img src="/img/icons/arrow.svg" alt="Icon">
                </div>
                <ul class="sub_menu count-list">
                    <li class="list-item count-item" data-value="{{ \App\Models\Reservation::QUANTITY_ONE }}">{{ translate('registration.labels.quantity.one') }}</li>
                    <li class="list-item count-item" data-value="{{ \App\Models\Reservation::QUANTITY_COUPLE }}">{{ translate('registration.labels.quantity.not-one') }}</li>
                    <li class="list-item count-item" data-value="{{ \App\Models\Reservation::QUANTITY_UNKNOWN }}">{{ translate('registration.labels.quantity.unknown') }}</li>
                </ul>
            </div>
        </div>

        <input
                type="checkbox"
                id="agree-input"
                class="checkbox-input wow animate__animated animate__fadeInUp"
                name="agreement"
                required
                checked
        >
        <label for="agree-input" class="checkbox-label">
            {{ translate('registration.labels.agree') }}
        </label>

        <button
                type="submit"
                class="reg__submit wow animate__animated animate__fadeInUp"
                id="reservation-btn-js"
                data-type="submit"
                data-link="{{ route('tours.reservation') }}"
                data-notyf="{{ translate('registration.labels.notyf.registration') }}"
        >
            @if($is_past)
                {{ translate('registration.btn.submit-past') }}
            @elseif($is_no_place)
                {{ translate('registration.btn.submit-no-place') }}
            @else
                {{ translate('registration.btn.submit') }}
            @endif

        </button>
    </form>
</section>

<div
        id="reservationModal-js"
        data-label="{{ translate('registration.modal.label') }}"
        data-description="{{ translate('registration.modal.description') }}"
        data-button="{{ translate('registration.modal.button') }}"
></div>
