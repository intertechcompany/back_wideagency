@extends('layouts.app')

@section('style')
    <link href="{{ mix('css/blog.css') }}" rel="stylesheet">
@endsection

@section('content')
    @if($data['success'])
        @if(isset($data['data']['posts']) && !$data['data']['posts']->isEmpty())
            <!---------------------Section blog----------------------->
            <section class="blog blog-pad">
                <h1 class="blog__title animate__animated animate__fadeInUp">{{ translate('posts.labels.past.tours') }}</h1>
                <ul class="blog__list">

                    @foreach($data['data']['posts'] as $post)
                        <!--------------Blog item--------------->
                        <li class="wow animate__animated animate__fadeInUp">
                            <a href="{{ localized_url(route('posts.page', ['slug' => $post->slug])) }}" class="blog__item">
                                <div class="blog__img-wrap">
                                <img src="{{ null === $post->poster ? '' : env('BACKEND_CORS_ORIGIN_URL') . $post->poster->path }}" class="blog__img" alt="{{ $post->poster->title ?? '' }}">
                                </div>
                                    <div class="blog__item-info">
                                    <div class="blog__item-date">{{ \App\Services\Helpers\DataHelper::getMonth($post->created_at->format('m')) .', '. $post->created_at->format('Y') }}</div>
                                    <h3 class="blog__item-title">{{ $post->title }}</h3>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>

                <!-----------------------------Pagination---------------->
                {{ $data['data']['posts']->links('vendor.pagination.default') }}
            </section>
        @endif

        @if(isset($data['data']['tours']) && !$data['data']['tours']->isEmpty())
            <!-------------------Travels------------------->

            <section class="travels">
                <h2 class="travels__title wow animate__animated animate__fadeInUp">{{ translate('posts.labels.feature.tour') }}</h2>
                <ul class="travels__list">

                @foreach($data['data']['tours'] as $tour)
                    <!-------------Travel item----------------->
                        <li class="travels__item wow animate__animated animate__fadeInUp">
                            <a href="{{ localized_url(route('tours.page', ['slug' => $tour->slug])) }}">
                                <div class="travels__item-img-wrap">
                                <img src="{{ null === $tour->main_banner ? '' : env('BACKEND_CORS_ORIGIN_URL') . $tour->main_banner->path }}"
                                     alt="{{ $tour->main_banner->title ?? '' }}" class="travels__item-img">
                                </div>
                                <div class="travels__item-descr">
                                    <h4 class="travels__item-title"><span class="underline">{{ $tour->title }}</span></h4>
                                    <div class="travels__item-date">
                                        wine tour<span class="icon">→</span>{{ $tour->tour_date }}
                                    </div>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <a href="{{ localized_url(route('tours')) }}" class="travels__link"><span>→</span>{{ translate('posts.btn.show.tours') }}</a>
            </section>
        @endif
    @endif
@endsection
