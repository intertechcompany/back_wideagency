@extends('layouts.app')

@section('style')
    <link href="{{ mix('css/article.css') }}" rel="stylesheet">
@endsection

@section('content')
    @if($data['success'])
        @if(isset($data['data']['post']))

            <article class="article">
                <div class="article__date">{{ \App\Services\Helpers\DataHelper::getMonth($data['data']['post']->created_at->format('m')) .', '. $data['data']['post']->created_at->format('Y') }}</div>

                <!---------------------Header----------------->
                <div class="article__header animate__animated animate__fadeInUp">
                    <h1 class="article__title">
                        {{ $data['data']['post']->title }}
                    </h1>
                    <p class="article__header-descr">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['post']->short_description ?? '') !!}
                    </p>
                </div>

                <!-------------------Article description-------------->
                <div class="article-descr">

                    <!-----------------Article images---------------->
                    <div class="article__images-wrap">
                        @if( null != $data['data']['post']->image_1)
                            <img src="{{ null === $data['data']['post']->image_1 ? '' : env('BACKEND_CORS_ORIGIN_URL') .$data['data']['post']->image_1->path }}" alt="{{ $data['data']['post']->image_1->title ?? '' }}" class="article__images_big">
                        @endif
                        @if( null != $data['data']['post']->image_2)
                            <img src="{{ null === $data['data']['post']->image_2 ? '' : env('BACKEND_CORS_ORIGIN_URL') .$data['data']['post']->image_2->path }}" alt="{{ $data['data']['post']->image_2->title ?? ''}}" class="article__images_small">
                        @endif
                    </div>

                    <p class="article__header-descr article-descr__title wow animate__animated animate__fadeInUp">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media',$data['data']['post']->label_description_1 ?? '') !!}
                     </p>

                     <p class="article-descr__item wow animate__animated animate__fadeInUp">
                         {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['post']->description_1 ?? '') !!}
                    </p>

                    <!------------------Image--------------->

                    @if( null != $data['data']['post']->image_3)
                        <img src="{{ null === $data['data']['post']->image_3 ? '' : env('BACKEND_CORS_ORIGIN_URL') .$data['data']['post']->image_3->path }}" alt="{{ $data['data']['post']->image_3->title ?? '' }}" class="article-descr__img wow animate__animated animate__fadeInUp">
                    @endif

                    <p class="article-descr__img-title wow animate__animated animate__fadeInUp">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media',$data['data']['post']->label_description_2 ?? '') !!}
                    </p>

                    <p class="article-descr__item wow animate__animated animate__fadeInUp">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media',$data['data']['post']->description_2 ?? '') !!}
                    </p>

                    <!----------------Quotes------------------->
                    @if(isset($data['data']['post']->quote, $data['data']['post']->author) && !empty($data['data']['post']->quote) && !empty($data['data']['post']->author))
                        <div class="article-descr__quotes wow animate__animated animate__fadeInUp">
                            <blockquote class="article-descr__quotes-item">“{!! $data['data']['post']->quote !!}”</blockquote>
                            <div class="article-descr__quotes-author">— {!! $data['data']['post']->author !!} </div>
                        </div>
                    @endif

                    <p class="article-descr__item wow animate__animated animate__fadeInUp">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['post']->description_3 ?? '') !!}
                    </p>

                    @if(isset($data['data']['post']->slider) && !empty(json_decode($data['data']['post']->slider)))
                        <!-------------------------Gallery------------------>
                        <div class="article-gallery__wrap wow animate__animated animate__fadeInUp">
                            <div class="article-gallery">
                                @foreach(json_decode($data['data']['post']->slider) as $y => $slider)
                                    <img src="{{ env('BACKEND_CORS_ORIGIN_URL') . env('STORAGE_URL') . '/media'.  $slider->slider_images }}" alt="{{ $slider->slider_images_alt ?? '' }}" class="article-gallery__img">
                                @endforeach
                            </div>

                            <div class="article-gallery__count"></div>
                        </div>
                    @endif

                    <h4 class="article-descr__img-title wow animate__animated animate__fadeInUp">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['post']->label_description_4 ?? '') !!}
                    </h4>

                    <p class="article-descr__item wow animate__animated animate__fadeInUp">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['post']->description_4 ?? '') !!}
                    </p>

                    <!-----------------Article images---------------->
                    <div class="article__images-wrap article__images_mar">
                        @if( null != $data['data']['post']->image_4)
                            <img src="{{ null === $data['data']['post']->image_4 ? '' : env('BACKEND_CORS_ORIGIN_URL') .$data['data']['post']->image_4->path }}" alt="{{ $data['data']['post']->image_4->title ?? ''}}" class="article__images_big animate__animated animate__fadeInUp">
                        @endif
                        @if( null != $data['data']['post']->image_5)
                            <img src="{{ null === $data['data']['post']->image_5 ? '' : env('BACKEND_CORS_ORIGIN_URL') .$data['data']['post']->image_5->path }}" alt="{{ $data['data']['post']->image_5->title ?? '' }}" class="article__images_small animate__animated animate__fadeInUp">
                        @endif
                    </div>

                    <p class="article-descr__item wow animate__animated animate__fadeInUp">
                        {!! str_replace('src="/storage/app/media', 'src="'. env('BACKEND_CORS_ORIGIN_URL') .'/storage/app/media', $data['data']['post']->description_5 ?? '') !!}
                    </p>
                </div>
            </article>

            @if(null !== $data['data']['next'] || null !== $data['data']['previous'])
                <!-------------------Swithcing ------------->
                <section class="switch-wrap wow animate__animated animate__fadeInUp">
                    <div class="switch">
                        @if(null !== $data['data']['next'])
                            <div class="switch__prev">
                                <a href="{{ localized_url(route('posts.page', ['slug' => $data['data']['next']->slug])) }}" class="switch__link">← <span>{{ translate('post.btn.next.post') }}</span></a>
                                <h2 class="switch__title">{{ $data['data']['next']->title }}</h2>
                            </div>
                        @endif

                        @if(null !== $data['data']['previous'])
                            <div class="switch__next">
                                <a href="{{ localized_url(route('posts.page', ['slug' => $data['data']['previous']->slug])) }}" class="switch__link">→ <span>{{ translate('post.btn.previous.post') }}</span></a>
                                <h2 class="switch__title">{{ $data['data']['previous']->title }}</h2>
                            </div>
                        @endif
                    </div>
                </section>
            @endif
        @endif
    @endif
@endsection

@section('scripts')
    <script defer src="{{ mix('js/article.js') }}"></script>
@endsection