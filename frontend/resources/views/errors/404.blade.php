@extends('layouts.app')

@section('style')
    <link href="{{ mix('css/404.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section class="info">
        <img src="/img/404.svg" alt="Not found image" class="info__img">
        <h1 class="info__title">{{ translate('page.404.title') }}</h1>
        <div class="info__descr">
            <div>{{ translate('page.404.description') }}</div>
            <span>{{ translate('page.404.sub-description') }} → </span><a href="mailto:{{ translate('page.404.mail') }}" class="info__email">{{ translate('page.404.mail') }}</a>
        </div>
        <a href="{{ route('home.page') }}" class="info__link">{{ translate('page.404.link.home') }}</a>
    </section>
@endsection
