<html>
    <head>
        <style>
            .review-table {
                border-collapse: collapse;
                width: 100%;
            }

            .review-table td, .review-table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
        </style>

    </head>

    <body>
        <table class="review-table">
            <caption></caption>
            <thead>

            <tr>
                <th scope="col" data-label="Ф.И.О">Ф.И.О</th>
                <th scope="col" data-label="Телефон">Телефон</th>
                <th scope="col" data-label="Email">Email</th>
                <th scope="col" data-label="Количество людей">Количество людей</th>
                <th scope="col" data-label="Комментарий">Комментарий</th>
            </tr>

            </thead>
            <tbody>

            <tr>
                <td data-label="Ф.И.О">{{ $reservation->name }}</td>
                <td data-label="Телефон">{{ $reservation->phone }}</td>
                <td data-label="Email">{{ $reservation->email }}</td>
                <td data-label="Количество людей">{{ \App\Models\Reservation::QUANTITY_ONE === $reservation->quantity ? 'Один' : 'Пара' }}</td>
                <td data-label="Комментарий">{{ $reservation->comment }}</td>
            </tr>

            </tbody>
        </table>
    </body>
</html>

