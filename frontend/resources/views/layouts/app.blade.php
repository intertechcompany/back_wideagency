@php
    $seo = null;

    if(Request::has('seo')) {
        $seo = Request::get('seo');
    }
@endphp

{{--@spaceless--}}
    <!doctype html>
    <html lang="{{ Localization::getCurrentLocale() }}">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

            {!! SEO::head() !!}

            <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
            <meta name="csrf-token" content="%%csrf_token%%">
            <link href="{{ mix('css/app.css') }}" rel="stylesheet">
            @yield('style')

            @if(null !== $seo)
                @if(!$seo->is_indexing)
                    <meta name="robots" content="noindex, follow">
                @endif
            @endif
        </head>

        <body>
        {!! SEO::bodyTop() !!}

        @include('partials._header')


        <div id="app" v-bind:class="{ 'is-hover': isHover, 'is-scroll-down': isScrollDown }">
            @yield('content')
        </div>

        @include('partials._footer')

        {!! SEO::bodyBottom() !!}

        <script defer src="{{ mix('js/app.js') }}"></script>
        @yield('scripts')
        </body>
    </html>
{{--@endspaceless--}}